/**
 * Pagean test framework.
 *
 * @module pagean
 */

import puppeteer from 'puppeteer';

import * as testFunctions from './lib/tests.js';
import { createLinkChecker } from './lib/link-utils.js';
import { saveReports } from './lib/reporter.js';
import testLogger from './lib/logger.js';

/**
 * @typedef  {object} PageOptions
 * @property {number} retryCount  - The number of times to retry navigation.
 * @property {number} timeout     - The timeout for navigation.
 */

/** @type {PageOptions} */
const defaultPageOptions = {
    retryCount: 2,
    timeout: 60_000
};

/** @typedef {import('puppeteer').Page} Page */

/**
 * Opens the specified URL in the given page, with retry logic.
 *
 * @param  {Page}        page    The puppeteer page object.
 * @param  {string}      url     The URL to navigate to.
 * @param  {PageOptions} options The options for navigating to the URL.
 * @throws {Error}               If the navigation fails.
 * @static
 * @private
 */
const gotoPage = async (page, url, options) => {
    let retryCount = 0;
    let lastError;
    while (retryCount < options.retryCount) {
        try {
            /* eslint-disable no-await-in-loop -- loop for retry only */
            // User provided test input required.
            // nosemgrep: puppeteer-goto-injection
            await page.goto(url, {
                timeout: options.timeout,
                waitUntil: 'domcontentloaded'
            });
            /* eslint-enable no-await-in-loop -- loop for retry only */
            break;
        } catch (error) {
            lastError = error;
            retryCount++;
        }
    }

    // Normalize URLs for comparison, prevents cases like trailing slashes
    // from causing false negatives.
    const desiredUrl = new URL(url);
    const currentUrl = new URL(page.url());
    if (currentUrl.href !== desiredUrl.href) {
        throw lastError;
    }
};

/** @typedef {import('./lib/config.js').PageanConfig} PageanConfig */

/* eslint-disable no-await-in-loop, max-lines-per-function --
   no-await-in-loop - tests are deliberately performed synchronously,
   max-lines-per-function - allowed to test executor */
/**
 * Executes Pagean tests as specified in config and reports results.
 *
 * @param {PageanConfig} config The Pagean test configuration.
 * @static
 */
// eslint-disable-next-line complexity -- Allow low limit
const executeAllTests = async (config) => {
    const logger = testLogger(config);
    const linkChecker = createLinkChecker();
    const browser = await puppeteer.launch(config.puppeteerLaunchOptions);

    for (const testUrl of config.urls) {
        logger.startUrlTests(testUrl.rawUrl);

        const consoleLog = [];
        const page = await browser.newPage();
        page.on('console', (message) =>
            consoleLog.push({
                location: message.location(),
                text: message.text(),
                type: message.type()
            })
        );

        try {
            await gotoPage(page, testUrl.url, defaultPageOptions);
        } catch (error) {
            logger.logPageError(error.message);
            /* eslint-disable-next-line no-continue -- tests should not be
               executed if navigation fails */
            continue;
        }

        const testContext = {
            consoleLog,
            linkChecker,
            logger,
            page,
            urlSettings: {
                ...testUrl.settings,
                htmlHintConfig: config.htmlHintConfig
            }
        };
        for (const test of Object.keys(testFunctions)) {
            await testFunctions[test](testContext);
        }

        await page.close();
    }
    await browser.close();
    const testResults = logger.getTestResults();
    saveReports(testResults, config.reporters);

    if (testResults.summary.failed > 0 || testResults.summary.pageError > 0) {
        // For test harness want process to exit with error code
        // eslint-disable-next-line unicorn/no-process-exit, no-magic-numbers -- exit code
        process.exit(2);
    }
};
/* eslint-enable no-await-in-loop, max-lines-per-function -- enable */

export { executeAllTests };
