import { coverageConfigDefaults, defineConfig } from 'vitest/config';

const timeout = 60_000;

export default defineConfig({
  cacheDir: '.vite',
  test: {
    chaiConfig: {
      includeStack: true
    },
    coverage: {
      exclude: [
        '**/bin/**',
        '**/coverage/**',
        '**/pagean-external-scripts/**',
        ...coverageConfigDefaults.exclude
      ],
      include: ['**'],
      reporter: ['cobertura', 'html', 'json', 'text'],
      thresholds: {
        statements: 90,
        branches: 90,
        functions: 90,
        lines: 90
      }
    },
    exclude: ['**/Archive/**', '**/node_modules/**'],
    hookTimeout: timeout,
    outputFile: 'junit.xml',
    reporters: ['verbose', 'junit'],
    testTimeout: timeout
  }
});
