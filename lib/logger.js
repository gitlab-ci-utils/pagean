/**
 * Creates an instance of a logger object to manage logging functions.
 *
 * @module logger
 */

import consoleLogger from 'ci-logger';

import { reporterTypes } from '../lib/reporter.js';

const testResultSymbols = Object.freeze({
    failed: ' ×',
    pageError: ' ×',
    passed: ' √',
    warning: ' ‼'
});

const nullFunction = () => {};

/** @typedef {import('./lib/config.js').PageanConfig} PageanConfig */

/**
 * Factory function that creates an instance of a
 * logger object to manage logging functions.
 *
 * @param   {PageanConfig} config Pagean configuration.
 * @returns {object}              A logger object.
 * @static
 */
// eslint-disable-next-line max-lines-per-function -- factory function with state
const factory = (config) => {
    const cliReporter = {
        // If cli reporter is enabled, set console log function, otherwise null function
        log: config.reporters.includes(reporterTypes.cli)
            ? consoleLogger.log
            : nullFunction,
        // The logAlways function provides a mechanism to log to the console even
        // when the cli reporter is disabled (e.g. returning the final results)
        logAlways: consoleLogger.log
    };

    const testResults = {
        executionStart: new Date(),
        project: config.project,
        results: [],
        summary: {
            failed: 0,
            pageError: 0,
            passed: 0,
            tests: 0,
            warning: 0
        }
    };

    let currentUrlTests;
    /**
     * Indicates the start of testing the specified URL. Subsequent
     * calls to logTestResults will be logged with this URL.
     *
     * @instance
     * @param {string} url The URL being tested.
     */
    const startUrlTests = (url) => {
        const urlTestResults = {
            tests: [],
            url
        };
        testResults.results.push(urlTestResults);
        currentUrlTests = urlTestResults;
        cliReporter.log({ message: `\nTesting URL: ${url}` });
    };

    /**
     * Logs an error navigating to the URL under test.
     *
     * @instance
     * @param {string} message The page error message to log.
     */
    const logPageError = (message) => {
        currentUrlTests.pageError = message;
        testResults.summary.pageError++;

        cliReporter.log({
            isResult: true,
            message: `Failed to navigate to URL: ${message}`,
            resultPrefix: testResultSymbols.pageError
        });
    };

    /**
     * Logs test results for the current URL (indicated
     * by the last call to StartUrlTests).
     *
     * @instance
     * @param {object} testResult The test results object.
     */
    const logTestResults = (testResult) => {
        if (testResult) {
            currentUrlTests.tests.push(testResult);
            testResults.summary.tests++;
            testResults.summary[testResult.result]++;

            cliReporter.log({
                isResult: true,
                message: `${testResult.name} (${testResult.result})`,
                resultPrefix: testResultSymbols[testResult.result]
            });
        }
    };

    const outputTestSummary = () => {
        cliReporter.logAlways({
            message: `\n Tests: ${testResults.summary.tests}\n Passed: ${testResults.summary.passed}\n Warning: ${testResults.summary.warning}\n Failed: ${testResults.summary.failed}\n Page Errors: ${testResults.summary.pageError}\n`
        });
    };

    /**
     * Outputs the test results summary to stdout and returns
     * the consolidated results for all tests.
     *
     * @instance
     * @returns {object} The consolidated results for all tests.
     */
    const getTestResults = () => {
        outputTestSummary();
        return testResults;
    };

    return {
        getTestResults,
        logPageError,
        logTestResults,
        startUrlTests
    };
};

export default factory;
