# Changelog

## v13.3.2 (2025-03-04)

### Fixed

- Updated to `puppeteer@24.3.1`, which includes security patches to Chrome 133.
- Updated the container base image to the latest Node.js v22.14.0 security patch.

## v13.3.1 (2025-02-27)

### Fixed

- Updated to `puppeteer@24.3.0`, which includes security patches to Chrome 133
  and Firefox 135.
- Update to `axios@1.8.1`.
- Updated the container base image to the latest Node.js v22.14.0 security patch.

## v13.3.0 (2025-02-16)

### Changed

- Updated to `puppeteer@24.2.1`, which updates to Chrome 133 and Firefox 135.

### Fixed

- Updated the container base image to Node.js v22.14.0.

### Miscellaneous

- Updated `pnpm` tests with settings to run `puppeteer` post-install script to
  install the browser. All lifecycle scripts are disabled by default in `pnpm`
  v10. (#199)

## v13.2.1 (2025-02-02)

### Fixed

- Updated to `puppeteer@24.1.1`, which includes security patches to Chrome 132
  and Firefox 134.
- Updated to `commander@13.0.1`.

## v13.2.0 (2025-01-17)

### Changed

- Updated to `puppeteer@24.1.0`, which includes updating to Chrome 132
  and Firefox 134.
  - Removes deprecated launch option interface, which should not be breaking as
    the new interface only adds capabilities. See the
    [puppeteer documentation](https://pptr.dev/api/puppeteer.launchoptions) for
    complete details.

### Fixed

- Updated the container base image to Node.js v22.13.0.

## v13.1.1 (2025-01-07)

### Fixed

- Updated to latest dependencies (`commander@13.0.0`), with minor refactoring
  for breaking changes.

## v13.1.0 (2024-12-19)

### Changed

- Updated to `puppeteer@23.11.1`, which includes security patches to Chrome 131
  and updating to Firefox 133.

## v13.0.3 (2024-12-07)

### Fixed

- Updated to latest dependencies (`puppeteer@23.10.1`, `axios@1.7.9`).
- Updated container base image to Node.js v22.12.0.

## v13.0.2 (2024-11-29)

### Fixed

- Updated to latest dependencies (`axios@1.7.8`).

## v13.0.1 (2024-11-22)

### Fixed

- Updated to `puppeteer@23.9.0`, which includes security patches to Chrome 131.

## v13.0.0 (2024-11-15)

### Changed

- BREAKING: Updated container base image to Node.js v22.11.0, which is the
  Active LTS release as of 2024-10-29. (#197)
- Updated to `puppeteer@23.8.0`, which includes updating to Chrome 131.

### Miscellaneous

- Updated CI pipeline jobs using Node LTS to v22.11.0.

## v12.3.0 (2024-11-08)

### Changed

- Updated to `puppeteer@23.7.1`, which includes updating to Firefox 132 and
  security patches to Chrome 130.

## v12.2.1 (2024-10-31)

### Fixed

- Updated to `puppeteer@23.6.1`, which includes security patches to Chrome 130.

## v12.2.0 (2024-10-19)

### Changed

- Updated to `puppeteer@23.6.0`, which includes updating to Chrome 130.

## v12.1.3 (2024-10-10)

### Fixed

- Updated to `puppeteer@23.5.2`.
- Updated to `@aarongoldenthal/eslint-config-standard@32.0.0` and resolve lint
  issues.

## v12.1.2 (2024-10-06)

### Fixed

- Updated to `puppeteer@23.5.0`.
- Updated container base image to Node 20.18.0.

## v12.1.1 (2024-09-29)

### Fixed

- Updated to `puppeteer@23.4.1`, which includes security patches to Chrome 129.
- Fixed CI pipeline to update OCI image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `puppeteer` image
  were cascading to this image.

## v12.1.0 (2024-09-18)

### Changed

- Updated to `puppeteer@23.4.0` (which includes updating to Chrome 129).

## v12.0.3 (2024-09-09)

### Fixed

- Updated to `puppeteer@23.3.0` and `axios@1.7.7`.

## v12.0.2 (2024-08-29)

### Fixed

- Updated to `puppeteer@23.2.1`.
- Updated `needs` for `npm_publish` job to include all required checks. (#195)

## v12.0.1 (2024-08-27)

### Fixed

- Updated container image to Node v20.17.0.
- Updated to `puppeteer@23.2.0` (which includes updating to Chrome 128) and
  `axios@1.7.5`.

## v12.0.0 (2024-08-20)

### Changed

- BREAKING: Updated container base image to new
  [Puppeteer](https://gitlab.com/gitlab-ci-utils/container-images/puppeteer)
  project and Debian Bookworm.

### Fixed

- Updated to `puppeteer@23.1.0`.

## v11.2.1 (2024-08-13)

### Fixed

- Updated to `axios@1.7.4` to resolve
  [CVE-2024-39338](https://nvd.nist.gov/vuln/detail/CVE-2024-39338) /
  [GHSA-8hc4-vh64-cxmj](https://github.com/advisories/GHSA-8hc4-vh64-cxmj).

## v11.2.0 (2024-08-11)

### Changed

- Updated to `puppeteer@23.0.2`, which includes updating to Chrome 127.
  - Chrome 127 was failing when running in GitLab CI under the SYSTEM account,
    so a Windows-specific test config was added. (#193)

### Fixed

- Updated container image to Node v20.16.0.
- Updated to `axios@1.7.3`.

## v11.1.5 (2024-07-20)

### Fixed

- Updated to latest dependencies (`puppeteer@22.13.1`, `ajv@8.17.1`).

## v11.1.4 (2024-07-11)

### Fixed

- Updated to `puppeteer@22.13.0`.
- Updated container image to Node v20.15.1.

## v11.1.3 (2024-06-29)

### Fixed

- Updated to `puppeteer@22.12.1`.
- Updated container image to Node v20.15.0.

## v11.1.2 (2024-06-21)

### Fixed

- Updated to `puppeteer@22.12.0`.

## v11.1.1 (2024-06-18)

### Fixed

- Updated to `puppeteer@22.11.2`.

## v11.1.0 (2024-06-13)

### Changed

- Updated to `puppeteer@22.11.0`, which updates to Chrome 126.

## v11.0.4 (2024-06-11)

### Fixed

- Updated to latest dependencies (`ajv@8.16.0`, `puppeteer@22.10.1`).

## v11.0.3 (2024-06-03)

### Fixed

- Updated to latest dependencies (`ajv@8.15.0`).
- Update to ESLint v9 with flat config and
  [rules](https://gitlab.com/gitlab-ci-utils/eslint-config-standard/-/releases)
  v28, which required minor refactoring for several new lint issues.

## v11.0.2 (2024-05-29)

### Fixed

- Updated to latest dependencies (`ajv@8.14.0`, `puppeteer@22.10.0`).

## v11.0.1 (2024-05-21)

### Fixed

- Updated to `axios@1.7.2`.

### Miscellaneous

- Updated to Renovate config v1.1.0.

## v11.0.0 (2024-05-18)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#190)
- BREAKING: Updated the included `.pageanrc.json` JSON schema to
  [draft 2019-09](https://json-schema.org/draft/2019-09/release-notes).
- Updated package to pure ESM. This is not considered BREAKING since only the
  CLI interface is exposed. There is no functional change, but the order of the
  tests for each URL is now slightly different (the CJS package used the order
  of test exports, which was arbitrary, but the ESM package orders the tests by
  the function names). The documentation and example reports have been updated
  to reflect the new test execution order. (#120)
- Updated container image from Node.js 20.12.2 to 20.13.1.
- Updated to `puppeteer@22.9.0`, which updates to Chrome 125.

### Fixed

- Updated page load to properly handle page load errors. Page load will be
  retried up to two times (per #187), and if page is not loaded the failure is
  logged as a "page error". These errors are included in all 3 reports (`cli`,
  `json`, and `html`). The Pagean job also fails for any page errors. See the
  [example reports](README.md#reports) for details (the last URL). (#145)
- Resolved timing issue with flaky integration test.
- Updated to latest dependencies (`ajv@8.13.0`, `ci-logger@7.0.0`,
  `commander@12.1.0`, `normalize-url@8.0.1`, and `protocolify@4.0.0`).

### Miscellaneous

- Updated `node_lts_test` CI job to include a Cobertura coverage report
  uploaded to GitLab. This allows line coverage to be viewed in GitLab.

## v10.2.0 (2024-04-25)

### Changed

- Updated to `puppeteer@22.7.1`, which updates to Chrome 124.

## v10.1.5 (2024-04-16)

### Fixed

- Updated to `puppeteer@22.6.5`.

## v10.1.4 (2024-04-13)

### Fixed

- Updated to Node 20.12.2, resolving [CVE-2024-27980](https://nodejs.org/en/blog/vulnerability/april-2024-security-releases-2#command-injection-via-args-parameter-of-child_processspawn-without-shell-option-enabled-on-windows-cve-2024-27980---high).

## v10.1.3 (2024-04-11)

### Fixed

- Updated to `puppeteer@22.6.4`.

## v10.1.2 (2024-04-05)

### Fixed

- Updated to `puppeteer@22.6.3`.

## v10.1.1 (2024-04-03)

### Fixed

- Updated to `puppeteer@22.6.2`.

### Changed

- Updated `container_build` job to use predefined variable for `pagean` version
  on tag pipelines instead of hardcoding value. (#150)

## v10.1.0 (2024-03-25)

### Changed

- Updated container images to Node 20 (no functional changes). (#187)
- Updated to `puppeteer@22.6.1`, which updates to Chrome 123.

### Fixed

- Updated tests to wait for the DOMContentLoaded event, which is more
  appropriate for the tests being run. Also increased the default
  page load timeout to 60s. (#187)
- Update page load to retry on error (retry up to 2 times). (#187)
- Removed `http-server` from the `latest` container image. This was
  done for versioned tags with v9.0.0, but was missed in the other Dockerfile.
  (#187)

## v10.0.2 (2024-03-16)

### Fixed

- Updated to latest dependencies (`axios@1.6.8`, `puppeteer@22.5.0`).
- Updated `create_release` job to `need` `deploy_tag` as well as `npm_publish`.
  (#186)

## v10.0.1 (2024-02-21)

### Fixed

- Updated to latest dependencies (`commander@12.0.0`, `puppeteer@22.1.0`).

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#185)

## v10.0.0 (2024-01-26)

### Changed

- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11) and added
  support for Node 21 (released 2023-10-17). Compatible with all current and
  LTS releases (^18.12.0 || >=20.0.0). (#181, #182)

### Fixed

- Updated to `axios@1.6.7` to resolve
  [CVE-2023-45857](https://github.com/axios/axios/issues/6006).
- Updated container image to Node 18.19.0.
- Updated to latest dependencies (`commander@11.1.0`, `handlebars@4.7.8`,
  `puppeteer@21.9.0`, `xml2jx@0.6.2`).
- Fixed tag pipeline `needs` issues. (#176, #177)
- Updated HTML report to specify all colors via CSS variables and use logical
  properties.

### Miscellaneous

- Updated `odc_external_scripts` job with `--noupdate` flag to avoid issues
  when NVD is down.
- Resolved previous SAST findings, all either false positives or understood
  risk for analysis tool. (#173)
- Updated default git branch to `main`. (#183)

## v9.0.0 (2023-07-04)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#171)
- BREAKING: Changed the default `puppeteer` `headless` mode to `new`, which
  will [soon be the default](https://developer.chrome.com/articles/new-headless/).
  To use the previous value set `"puppeteerLaunchOptions": { "headless": true }`
  in your `.pageanrc.json` file. (#172)
- BREAKING: Removed `http-server` from the container image. This was replaced
  with [`serve`](https://www.npmjs.com/package/serve) in v8.0.4. (#142)
- Updated the configuration to allow specifying a sitemap.xml file with URLs
  to test. See the
  [documentation](https://gitlab.com/gitlab-ci-utils/pagean#configuration) for
  details. (#164)

### Fixed

- Updated HTML report show/hide details icon to render properly on all
  platforms. (#175)
- Updated to latest dependencies (`axios@1.4.0`, `ci-logger@6.0.0`,
  `commander@11.0.0`, `puppeteer@20.7.4`).

### Miscellaneous

- Updated pipeline to optimize execution. (#174)

## v8.0.4 (2023-02-19)

### Changed

- Added [`serve`](https://www.npmjs.com/package/serve) as a replacement static
  server in the container image. The `http-server` capability is deprecated
  and it will be removed from the image in v9.0.0.

### Fixed

- Fixed broken link test to properly escape URL hash values to cover
  characters that are valid in DOM element IDs, but must be escaped in
  `queryselector` or the element reports a `999` status. (#168)
- Fixed error in npm `start-lint-all` script, preventing all `pageanrc` files
  from being properly linted. (#169)
- Updated to latest dependencies (`axios@1.3.3`, `ci-logger@5.1.1`,
  `puppeteer@19.7.1`).

## v8.0.3 (2023-02-02)

### Fixed

- Fixed issue with tests failing on `file:` links or scripts with `axios@1.3.1`.
  Updated documentation to clarify behavior for `file:` URLs in the broken
  link test. (#166)
- Update to latest dependencies (`axios@1.3.1`, `puppeteer@19.6.3`).

### Miscellaneous

- Update CI pipeline to remove exceptions for `schedule` pipelines.

## v8.0.2 (2023-01-15)

### Fixed

- Update to latest dependencies (`ajv@8.12.0`, `axios@1.2.2`, `commander@10.0.0`,
  `puppeteer@19.5.2`).
- Updated README with details on setting `PUPPETEER_CACHE_DIR` in the image to
  control the cache location for installing the Chrome binary. (#161)
- Clarified documentation that Broken Link Test will check all links on a page,
  and does not respect mechnisms to limit web crawlers (e.g. `robots.txt`). (#153)

### Miscellaneous

- Update `renovate` config to resolve issues managing
  `registry.gitlab.com/gitlab-ci-utils/docker-puppeteer` images. (#163)

## v8.0.1 (2022-11-04)

### Fixed

- Fixed Broken Link Test so that it properly accepts a compressed HEAD response when checking links. This was previously returning a `Z_BUF_ERROR` response due to [`axios` error](https://github.com/axios/axios/issues/5102). (#162)
- Increased page load time threshold to resolve failing integration test. (#154)
- Update to latest dependencies (`puppeteer@19.2.2`).

### Miscellaneous

- Updated `Dockerfile`s to reference Node base image by explicit tag (e.g. `18.12.0-bullseye-slim`).

## v8.0.0 (2022-11-01)

### Changed

- BREAKING: Changed container image to Node 18, which the Active LTS as of 2022-10-25. The base
  image was also updated to be based on Node Debian Slim vs Node Debian, see full details
  [here](https://gitlab.com/gitlab-ci-utils/docker-puppeteer/-/blob/master/docs/debain-vs-debian-slim.md). (#160)
- Added standard set of `LABEL`s to images (documentation, license, etc). (#157)
- Added tests for Node 19.

### Fixed

- Updated page load time tests to remove external resources. (#155)
- Updated `npm-check` ignore syntax for issue in v6.0.0. (#151)
- Updated `npm ci` arguments in `Local.Dockerfile` for production-only install in npm v8. (#158)
- Update to latest dependencies: `axios@1.1.3`, `ci-logger@5.1.0`, `commander@9.4.1`,
  `kleur@4.1.5`, `puppeteer@19.2.0`.
  - Updated `Dockerfile`s to override `PUPPETEER_CACHE_DIR` in puppeteer v19 to install chrome
    binary within the `node_modules` folder. Also made corresponding changes in container-based
    tests. See details [here](https://github.com/puppeteer/puppeteer/pull/9095). (!501)

### Miscellaneous

- Updated CI pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#156)

## v7.0.0 (2022-06-15)

### Changed

- BREAKING: Updated console log properties names to be more intuitive (`text`, `type`, and `location`).
- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#147, #148)

### Fixed

- Updated to latest dependencies, including `puppeteer` v14.4.0

### Miscellaneous

- Changed all CI pipeline references from `master` branch to `CI_DEFAULT_BRANCH`. (#146)
- Added test coverage threshold requirements (#149)

## v6.0.9 (2022-04-27)

### Fixed

- Updated to latest dependencies, including `htmlhint` v1.1.4 and `puppeteer` v13.6.0

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#144)
- Moved project back to GitLab flow branching model. (#141)

## v6.0.8 (2022-03-28)

### Fixed

- Update to `htmlhint` v1.1.3, resolving CVE-2020-28469
- Updated tests for `ajv` message changes in v8.11.0
- Refactored to comply with latest `eslint` rules
- Updated to latest dependencies, including resolving CVE-2021-44906 (dev only)

### Miscellaneous

- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md) (#140)

## v6.0.7 (2022-02-11)

### Fixed

- Update to latest dependencies, including resolving `follow-redirects` vulnerability CVE-2022-0536

## v6.0.6 (2022-01-26)

### Fixed

- Updated to latest dependencies, including resolving `node-fetch` vulnerability CVE-2022-0235

## v6.0.5 (2022-01-19)

### Miscellaneous

- Added reporter `pa11y-ci-reporter-cli-summary` to `pa11y-ci` job (#139)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v6.0.4 (2021-12-18)

### Fixed

- Updated to latest dependencies, including `puppeteer` v13 with Chromium 97

## v6.0.3 (2021-11-18)

### Fixed

- Fixed issue with `secret_detection` job failing in some cases (#136)
- Updated to latest dependencies, including latest Node 16 image

## v6.0.2 (2021-11-17)

### Fixed

- Updated to latest dependencies, including latest Node 16 image

## v6.0.1 (2021-11-11)

### Fixed

- Fixed formatting of HTML report to wrap ling lines in test error/warning data and highlight background. (#134, #135)
- Updated to latest dependencies

## v6.0.0 (2021-11-07)

### **Security Notice**

- Pagean leverages [HTMLHint](https://github.com/htmlhint/HTMLHint) for the Rendered HTML Test. One of its child dependencies, `glob-parent`, includes a regular expression denial of service (ReDoS) vulnerability, tracked as [CVE-2020-28469](https://nvd.nist.gov/vuln/detail/CVE-2020-28469), [CWE-400](https://ossindex.sonatype.org/vulnerability/64cd5f21-8af4-4eae-ac7d-a53241ea693a), and a [GitHub Advisory](https://github.com/advisories/GHSA-ww39-953v-wcq6). This vulnerability has been assessed and only affects the CLI interface to HTMLHint. Pagean only uses the programmatic API for HTMLHint and is not susceptible to this vulnerability, even though it is identified through various scanners simply based on the included dependency. Resolution of the issue is tracked in this [Pagean issue](https://gitlab.com/gitlab-ci-utils/pagean/-/issues/118).

### Changed

- BREAKING: Changed exit code to 2 for any Pagean test error to differentiate from a test execution error, which will still result in exit code 1. (#125)
- BREAKING: Updated broken links test to cache results and not re-test duplicate links. Added configuration option `ignoreDuplicates` that can be used to bypass this behavior. (#112)
- BREAKING: Updated docker image to Node 16, which is now the active LTS release (#127)
- Added `checkWithBrowser` configuration option for the Broken Link Test that test links by loading them in the browser to address some false failure cases. Note this can increase test execution time, in some cases substantially, due to the time to open a new browser tab and plus load the page and all assets. (#105)

### Fixed

- Updated to latest dependencies (#102)
- Updated rules for scheduled pipelines to build from correct `Dockerfile`, and only run required jobs for security scans and other schedule-only checks. (#124, #133)
- Refactored tests for per updated lint rules

### Miscellaneous

- Configured [renovate](https://docs.renovatebot.com/) for dependency updates (#119, #131)
- Update test suite to include node 17 (!149, #126, #130)
- Moved to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#121)
- Updated project CSS and HTML lint configurations, with associated fixes (#128, #129)

## v5.0.0 (2021-07-25)

**See the [version upgrade guide](https://gitlab.com/gitlab-ci-utils/pagean/-/blob/master/docs/upgrade-guide.md) for details on breaking changes and instructions on upgrading from v4.x to v5.**

### Changed

- BREAKING: Changed Broken Link Test default to fail (without warning) to be consistent with other tests. (#83)
- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#108, #109)
- Updated Broken Link Test to ignore HTTPS errors if set in puppeteer launch options (#113)

### Fixed

- Updated error details in HTML report to show different arrows for open/closed state to match the default browser behavior (#114)
- Updated to latest dependencies
  - Resolved several vulnerabilities
  - Updated to [`htmlhint` v0.15.1](https://github.com/htmlhint/HTMLHint)
  - Update code to conform to latest `eslint` rules
- Updated Bootstrap link in test cases (#116)

### Miscellaneous

- Added note on known issues with broken link test, planned to be fixed in a future release (#84, #88)
- Optimize published package to only include the minimum required files (#104)
- Added Node v16 tests (#106)
- Updated CI pipeline for compatibility with the latest CI templates (#110, #111)

## v4.4.3 (2021-03-28)

### Fixed

- Resolved issues with `husky` trying to install in container (#99)
- Updated to latest dependencies (#102)

### Miscellaneous

- Updated CI pipeline to use needs for efficiency (#100)

## v4.4.2 (2021-03-21)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities and puppeteer v8 (#98)

### Miscellaneous

- Updated CI pipeline with multiple optimizations:
  - Changed `merge_request` pipelines to run instead of `push` pipelines when in a merge request
  - Moved all jobs from `only`/`except` to `rules` (#92)
  - Optimized pipeline for schedules (#93) and to use standard NPM package collection (#95)
  - Updated Pagean jobs to be included in release evidence collection (#96)
  - Updated Pagean jobs for release evidence collection (#96)
  - Updated Docker build to ensure latest published version is installed in tag pipelines (#89)
  - Refactor Docker build using rules to reduce to a single job, closes #94
  - Update Docker build rules for local vs npm installation (#34)
- Update to @aarongoldenthal/stylelint-config-standard (#91)

## v4.4.1 (2021-01-30)

### Fixed

- Fixed failure of global pagean install in container (#87)

## v4.4.0 (2021-01-30)

### Added

- Added test to check for broken links on the page (`http`/`file` only). For links within the page, checks for existence of the element on the page. For links to other pages, makes a request and checks response (fails if >= 400 or other error). Includes config update with array of links to ignore. Fails with warning by default so it is not a breaking change. (#77)
- Added [`http-server`](https://www.npmjs.com/package/http-server) and [`wait-on`](https://www.npmjs.com/package/wait-on) to Docker images and documented example showing how to test static files with a local server. (#59)

### Fixed

- Fixed error in console log reporting location information and updated tests (#85, #86)
- Fixed local installation on non-master branch Docker images to accept CLI arguments (#81)
- Updated to latest dependencies

### Miscellaneous

- Update CI pipeline to check for secure JSON schemas (#78) and lint `.pageanrc` files (#79)
- Fix test remnant left from adding `htmlhintrc` (#80)

## v4.3.0 (2020-12-28)

### Added

- Added CLI command to lint pageanrc configuration files (#74)
- Added configuration file property to specify project name for reports (#73)
- Added configuration file property to specify htmlhintrc file (#35)

### Changed

- Improved configuration file validation leveraging JSON schema (#75)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Refactored code to leverage array.map for optimization in several places (#70)

## v4.2.0 (2020-12-13)

### Added

- Added CLI option to specify configuration file location (#14)
- Added configuration file section to specify the reporters used to capture the test results (any or all of `cli`, `html`, or `json`) (#25)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Update test suite to include CLI integration tests (#63)
- Update pipeline for Node LTS change from v12 to v14 (#65)
- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#67) and GitLab Releaser (#68)

## v4.1.3 (2020-10-23)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Added tests with Node 15 (#64)

## v4.1.2 (2020-09-13)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v4.1.1 (2020-08-16)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v4.1.0 (2020-07-05)

### Changed

- Updated console output to improve visualization of test results (#61)

### Fixed

- Updated to latest dependencies, including Puppeteer 5

## v4.0.0 (2020-06-13)

**Note: As of v4.0.0, this module is being being released as [`pagean`](https://www.npmjs.com/package/pagean) and [`page-load-tests`](https://www.npmjs.com/package/page-load-tests) has been deprecated. See details and impacts below. This drove breaking configuration changes that will require updating your project's configuration file. See the [version upgrade guide](https://gitlab.com/gitlab-ci-utils/pagean/-/blob/master/docs/upgrade-guide.md) for complete details.**

### Added

- Added external script test to identify any externally loaded javascript files (e.g. loaded from a CDN) and aggregate those files so they can undergo further analysis (e.g. dependency vulnerability scanning). (#42)
- Added the ability to set a failed test to result in a warning instead of a failure, which will not cause failure of the test suite. (#44)

### Changed

- BREAKING: Updated test configuration to allow test-specific settings. With this change, `pageLoadTimeThreshold` must now appears as a setting of the `pageLoadTimeTest` test. (#54)
- BREAKING: Removed support for Node v13 since now end-of-life. (#56)
- BREAKING: Updated module name to `pagean`, including the following configuration and report name changes (#57):
  - Changed configurations file name from `.pltconfig.json` to `.pageanrc.json`
  - Changed report names from `page-load-test-results.*` to `pagean-results.*`.

### Fixed

- Updated to latest dependencies

## v3.0.0 (2020-04-20)

### Changed

- BREAKING: Removed [Jest](https://jestjs.io/) as the test runner and replaced with new a dedicated test runner optimized for these types of tests. This includes simplified HTML and JSON reports, removing code stack traces on error and replacing with more meaningful data, etc (#43, #48, #33, #49). This also enables future enhancements that a typical test framework could not support (e.g. #47, #44, #42).

### Fixed

- Updated to Puppeteer 3.0.0 with Chrome 81 (#41, #50)
- Updated to latest dependencies to resolve vulnerabilities

## v2.0.0 (2020-01-26)

### Changed

- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#39)
- Updated documentation with link to [example report](https://gitlab-ci-utils.gitlab.io/pagean/pagean-results.html) (#28)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added testing for all supported Node.js versions (#36)
- Added accessibility analysis for report via [pa11y-ci](https://www.npmjs.com/package/pa11y-ci) (#38)
- Updated project to use [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#37)

## v1.2.1 (2019-11-24)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.0 (2019-11-16)

### Changed

- Updated rendered HTML test to use local `.htmlhintrc` file if available (#17)

## v1.1.1 (2019-11-10)

### Fixed

- Updated tests to exit with error (i.e. non-zero) for test failure (#32, #31)
- Updated to latest dependencies (Puppeteer, eslint/jest/plugins) (#30)

### Miscellaneous

- Updated project to use custom eslint configuration module (#29)
- Updated CI pipeline to lint HTML and CSS

## v1.1.0 (2019-10-19)

### Changed

- Updated tests to show original URLs (without full file path) (#18)
- Added custom HTML report template with error formatting (#10)
- Updated Dockerfile to run via standard CLI command (#21)

## v1.0.1 (2019-10-17)

### Fixed

- Fixed error when running in a path with spaces (#26)

## v1.0.0 (2019-10-16)

Initial release
