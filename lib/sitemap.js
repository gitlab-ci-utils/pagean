/**
 * Process sitemap files.
 *
 * @module sitemap
 */

import fs from 'node:fs';

import axios from 'axios';
import { parseStringPromise } from 'xml2js';

/**
 * Gets a sitemap, via file path or URL, and returns the string contents.
 *
 * @param   {string}          url The URL or file path to the sitemap.
 * @returns {Promise<string>}     The string contents of the sitemap.
 * @throws  {Error}               Throws if the sitemap cannot be retrieved.
 * @static
 * @private
 */
const getSitemap = async (url) => {
    try {
        if (url.startsWith('http')) {
            const response = await axios.get(url);
            return response.data;
        }
        // Allow users to specify a local sitemap filename.
        // nosemgrep: eslint.detect-non-literal-fs-filename
        return fs.readFileSync(url, 'utf8');
    } catch {
        throw new Error(`Error retrieving sitemap "${url}"`);
    }
};

/**
 * Parses a sitemap string and returns an array of URLs.
 *
 * @param   {string}            sitemapXml The string contents of the sitemap.
 * @returns {Promise<string[]>}            The URLs from the sitemap.
 * @static
 * @private
 */
const parseSitemap = async (sitemapXml) => {
    const sitemapData = await parseStringPromise(sitemapXml);
    if (!sitemapData.urlset.url) {
        return [];
    }
    return sitemapData.urlset.url.map((url) => url.loc[0]);
};

/**
 * Removes excluded URLs from the list of URLs.
 *
 * @param   {string[]} urls    The URLs from the sitemap.
 * @param   {string[]} exclude The URLs to exclude (regular expressions).
 * @returns {string[]}         The non-excluded URLs from the sitemap.
 * @static
 * @private
 */
const removeExcludedUrls = (urls, exclude = []) => {
    // Allow URLs to be excluded by regular expression.
    // nosemgrep: eslint.detect-non-literal-regexp
    const excludeRegex = exclude.map((url) => new RegExp(url));
    return urls.filter((url) => {
        for (const excludeUrlRegex of excludeRegex) {
            if (excludeUrlRegex.test(url)) {
                return false;
            }
        }
        return true;
    });
};

/**
 * Replaces the "find" string with the "replace" string in each of the URLs.
 *
 * @param   {string[]} urls      The URLs to process.
 * @param   {string}   [find]    The string to find in the URLs.
 * @param   {string}   [replace] The string to replace in the URLs.
 * @returns {string[]}           The processed URLs.
 * @static
 * @private
 */
const replaceUrlStrings = (urls, find, replace) => {
    if (!find || !replace) {
        return urls;
    }
    return urls.map((url) => url.replace(find, replace));
};

/**
 * @typedef  {object}   SitemapOptions
 * @property {string}   url            The URL or file path to the sitemap.
 * @property {string}   [find]         The string to find in the URLs.
 * @property {string}   [replace]      The string to replace in the URLs.
 * @property {string[]} [exclude]      The URLs to exclude (regular expressions).
 */

/**
 * Retrieves a list of URLs from a sitemap, via a local file path or URL.
 * Options are available to exclude URLs from the results, and to find and
 * replace strings in the URLs (primarily intended for isolated testing,
 * for example change "https://somewhere.test/" to "http://localhost:3000/").
 *
 * @param   {SitemapOptions}    options The sitemap processing options.
 * @returns {Promise<string[]>}         The processed URLs from the sitemap.
 * @throws  {Error}                     Throws if the sitemap cannot be processed.
 * @static
 * @public
 */
const getUrlsFromSitemap = async ({ url, find, replace, exclude } = {}) => {
    const sitemapXml = await getSitemap(url);
    try {
        const rawUrls = await parseSitemap(sitemapXml);
        return replaceUrlStrings(
            removeExcludedUrls(rawUrls, exclude),
            find,
            replace
        );
    } catch {
        throw new Error(`Error processing sitemap "${url}"`);
    }
};

export { getUrlsFromSitemap };
