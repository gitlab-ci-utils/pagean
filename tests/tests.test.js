import fs from 'node:fs';
import path from 'node:path';

import { afterAll, afterEach, describe, expect, it, vi } from 'vitest';
import protocolify from 'protocolify';
import puppeteer from 'puppeteer';

import * as fileUtils from '../lib/external-file-utils.js';
import * as linkUtils from '../lib/link-utils.js';
import { __dirname, readJson } from '../lib/utils.js';
import {
    brokenLinkTest,
    consoleErrorTest,
    consoleOutputTest,
    externalScriptTest,
    horizontalScrollbarTest,
    pageLoadTimeTest,
    renderedHtmlTest
} from '../lib/tests.js';
import testLogger from '../lib/logger.js';
import { testResultStates } from '../lib/test-utils.js';

const defaultConfig = await readJson(
    path.join(__dirname, '..', 'lib', 'default-config.json')
);

let browser, page;

const getPage = async (url) => {
    if (!browser) {
        const puppeteerLaunchOptions = {
            args: ['--no-sandbox'],
            ...defaultConfig.puppeteerLaunchOptions
        };
        // eslint-disable-next-line require-atomic-updates -- only assigned here
        browser = await puppeteer.launch(puppeteerLaunchOptions);
    }
    page = await browser.newPage();
    await page.goto(url, { waitUntil: 'load' });
};

// File is read each time instead of require to ensure original,
// not cached or modified cached, values.
const getDefaultSettings = () =>
    JSON.parse(fs.readFileSync('./lib/default-config.json', 'utf8')).settings;

const getContext = async (url, consoleLog, includePage = false) => {
    if (includePage) {
        await getPage(protocolify(url));
    }
    return {
        consoleLog,
        linkChecker: linkUtils.createLinkChecker(),
        logger: testLogger(defaultConfig),
        page,
        urlSettings: getDefaultSettings()
    };
};

const closePageAndBrowser = async () => {
    await page.close();
    /* eslint-disable-next-line require-atomic-updates -- only set here,
       called in after* hooks, and in tests. */
    page = undefined;
    await browser.close();
    /* eslint-disable-next-line require-atomic-updates -- only set here,
       called in after* hooks, and in tests. */
    browser = undefined;
};

const consoleLogWithoutErrors = [
    {
        location: {
            columnNumber: 16,
            lineNumber: 7,
            url: 'file:///C:/Users/someone/Documents/Projects/NodeJS/Page-Load-Tests/tests/fixtures/site/consoleLog.html'
        },
        text: 'This is a test',
        type: 'log'
    }
];

const consoleLogWithErrors = [
    {
        location: {
            url: 'https://this.url.does.not.exist/file.js'
        },
        text: 'Failed to load resource: net::ERR_NAME_NOT_RESOLVED',
        type: 'error'
    }
];

const executeTestWithLoggerSpy = async (
    context,
    testFunction,
    checkLinkResult = linkUtils.httpResponse.ok
) => {
    const loggerSpy = vi
        .spyOn(context.logger, 'logTestResults')
        .mockImplementation(() => {});
    const saveScriptSpy = vi
        .spyOn(fileUtils, 'saveExternalScript')
        .mockImplementation(() => {});
    const checkLinkSpy = vi
        .spyOn(context.linkChecker, 'checkLink')
        .mockImplementation(() => checkLinkResult);
    await testFunction(context);
    return {
        checkLinkSpy,
        loggerSpy,
        saveScriptSpy
    };
};

describe('consoleErrorTest', () => {
    const testConsoleErrorTest = async (context) =>
        executeTestWithLoggerSpy(context, consoleErrorTest);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    it('should not execute consoleErrorTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        context.urlSettings.consoleErrorTest.enabled = false;
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result if console log has no entries', async () => {
        expect.assertions(1);
        const context = await getContext('', []);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a passed test result if console log has entries without errors', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithoutErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result if console log has entries with errors', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with console log errors for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with console log errors for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithoutErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });
});

describe('consoleOutputTest', () => {
    const testConsoleOutputTest = async (context) =>
        executeTestWithLoggerSpy(context, consoleOutputTest);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    it('should not execute consoleOutputTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        context.urlSettings.consoleOutputTest.enabled = false;
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result if console log has no entries', async () => {
        expect.assertions(1);
        const context = await getContext('', []);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result if console log has entries', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithoutErrors);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with console log for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should log not data with console log for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext('', []);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });
});

describe('horizontalScrollbarTest', () => {
    const noHorizontalScrollbarUrl = 'tests/fixtures/site/consoleLog.html';
    const horizontalScrollbarUrl =
        'tests/fixtures/site/horizontalScrollbar.html';

    const testHorizontalScrollbarTest = async (context) =>
        executeTestWithLoggerSpy(context, horizontalScrollbarTest);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute horizontalScrollbarTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(noHorizontalScrollbarUrl, [], true);
        context.urlSettings.horizontalScrollbarTest.enabled = false;
        const { loggerSpy } = await testHorizontalScrollbarTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result for page with no horizontal scroll bar', async () => {
        expect.assertions(1);
        const context = await getContext(noHorizontalScrollbarUrl, [], true);
        const { loggerSpy } = await testHorizontalScrollbarTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result for page with a horizontal scroll bar', async () => {
        expect.assertions(1);
        const context = await getContext(horizontalScrollbarUrl, [], true);
        const { loggerSpy } = await testHorizontalScrollbarTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });
});

describe('pageLoadTimeTest', () => {
    const normalLoadTimeUrl = 'tests/fixtures/site/dynamicContent.html';
    const slowLoadTimeUrl = 'tests/fixtures/site/slowLoad.html';

    const testPageLoadTimeTest = async (context) =>
        executeTestWithLoggerSpy(context, pageLoadTimeTest);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute pageLoadTimeTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(normalLoadTimeUrl, [], true);
        context.urlSettings.pageLoadTimeTest.enabled = false;
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result for page that loads within timeout', async () => {
        expect.assertions(1);
        const context = await getContext(normalLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result and data with page load time for page that does not load within timeout', async () => {
        expect.assertions(1);
        const context = await getContext(slowLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with page load time for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext(slowLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({
                data: { pageLoadTime: expect.any(Number) }
            })
        );
    });

    it('should not log data with page load time for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext(normalLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({
                data: { pageLoadTime: expect.any(Number) }
            })
        );
    });
});

describe('renderedHtmlTest', () => {
    const validHtmlUrl = 'tests/fixtures/site/scriptError404.html';
    const invalidHtmlUrl = 'tests/fixtures/site/htmlError.html';

    const testRenderedHtmlTest = async (context) =>
        executeTestWithLoggerSpy(context, renderedHtmlTest);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute test if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(validHtmlUrl, [], true);
        context.urlSettings.renderedHtmlTest.enabled = false;
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result for page that has valid HTML', async () => {
        expect.assertions(1);
        const context = await getContext(validHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result for page that has invalid HTML', async () => {
        expect.assertions(1);
        const context = await getContext(invalidHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with HTML errors for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext(invalidHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with HTML errors for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext(validHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });
});

describe('externalScriptTest', () => {
    const noScriptUrl = 'tests/fixtures/site/consoleLog.html';
    const noExternalScriptUrl = 'tests/fixtures/site/noExternalScripts.html';
    const externalScriptUrl = 'tests/fixtures/site/externalScripts.html';

    const testExternalScriptTest = async (context) =>
        executeTestWithLoggerSpy(context, externalScriptTest);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute test if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(noScriptUrl, [], true);
        context.urlSettings.externalScriptTest.enabled = false;
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should execute test if enabled setting is set to true', async () => {
        expect.assertions(1);
        const context = await getContext(noScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        // Allow since this is just a configuration test to ensure execution, detailed
        // tests follow checking arguments.
        // eslint-disable-next-line vitest/prefer-called-with -- tested elsewhere
        expect(loggerSpy).toHaveBeenCalled();
    });

    it('should log a passed test result for page that has no scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a passed test result for page that has no external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noExternalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a warning test result for page that has external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(externalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.warning })
        );
    });

    it('should log data with HTML errors for a page that has external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(externalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with HTML errors for a page that does not have external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noExternalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should save files with external scripts for a page that has external scripts', async () => {
        expect.assertions(1);
        const externalFileCount = 3;
        const context = await getContext(externalScriptUrl, [], true);
        const { saveScriptSpy } = await testExternalScriptTest(context);
        expect(saveScriptSpy).toHaveBeenCalledTimes(externalFileCount);
    });

    it('should not save files with external scripts for a page that does not have external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noExternalScriptUrl, [], true);
        const { saveScriptSpy } = await testExternalScriptTest(context);
        expect(saveScriptSpy).not.toHaveBeenCalled();
    });
});

describe('brokenLinkTest', () => {
    const noLinkUrl = 'tests/fixtures/site/htmlError.html';
    const brokenLinkUrl = 'tests/fixtures/site/brokenLinks.html';
    const notDocumentLinksUrl = 'tests/fixtures/site/notDocumentLinks.html';
    const duplicateLinkUrl = 'tests/fixtures/site/duplicateLinks.html';

    const testBrokenLinkTest = async (context, checkLinkResult) =>
        executeTestWithLoggerSpy(context, brokenLinkTest, checkLinkResult);

    afterEach(() => {
        vi.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute broken links test if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        context.urlSettings.brokenLinkTest.enabled = false;
        const { loggerSpy } = await testBrokenLinkTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should execute broken links test if enabled setting is set to true', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ name: 'should not have broken links' })
        );
    });

    it('should log a passed test result for page that has no broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(
            context,
            linkUtils.httpResponse.ok
        );
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result for page that has broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        context.urlSettings.brokenLinkTest.failWarn = false;
        const { loggerSpy } = await testBrokenLinkTest(
            context,
            linkUtils.httpResponse.unknownError
        );
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with broken link errors for a page that has broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(
            context,
            linkUtils.httpResponse.unknownError
        );
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with broken link errors for a page that does not have broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should check for broken links for a page that has links', async () => {
        expect.assertions(1);
        const linkCount = 8;
        const context = await getContext(brokenLinkUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).toHaveBeenCalledTimes(linkCount);
    });

    it('should not check for broken links for a page that does not have links', async () => {
        expect.assertions(1);
        const context = await getContext(noLinkUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).not.toHaveBeenCalled();
    });

    it('should only check unique links for broken links', async () => {
        expect.assertions(1);
        // Test page includes 7 links with a duplicate same page link and external link
        const linkCount = 5;
        const context = await getContext(duplicateLinkUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).toHaveBeenCalledTimes(linkCount);
    });

    it('should not check for broken links for a page with only links that are not documents (http/file)', async () => {
        expect.assertions(1);
        const context = await getContext(notDocumentLinksUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).not.toHaveBeenCalled();
    });
});
