import path from 'node:path';

import {
    afterAll,
    afterEach,
    beforeAll,
    describe,
    expect,
    it,
    vi
} from 'vitest';
import axios from 'axios';
import protocolify from 'protocolify';
import puppeteer from 'puppeteer';

import { __dirname, readJson } from '../lib/utils.js';
import {
    createLinkChecker,
    httpResponse,
    isFailedResponse,
    normalizeLink
} from '../lib/link-utils.js';

const defaultConfig = await readJson(
    path.join(__dirname, '..', 'lib', 'default-config.json')
);

describe('link checker', () => {
    const externalLink = 'https://www.google.com';
    const fileLink = 'file:///C:/folder/file.html';
    const testUrl = './tests/fixtures/site/brokenLinks.html';
    const testUrlLink = protocolify(testUrl);
    const userAgent = 'pagean test user-agent';
    const puppeteerLaunchOptions = {
        args: ['--no-sandbox'],
        ignoreHTTPSErrors: false,
        ...defaultConfig.puppeteerLaunchOptions
    };
    let browser, page;

    const getContext = (
        checkWithBrowser,
        ignoredLinks = [],
        ignoreDuplicates = true
    ) => ({
        page,
        testSettings: {
            checkWithBrowser,
            ignoreDuplicates,
            ignoredLinks
        }
    });

    const createBrowserMock = (httpStatus, link, ignoreHTTPSErrors) => {
        const pageGotoMock = vi.fn(() => {
            if (Number.isNaN(Number(httpStatus))) {
                // If execution error, format to match puppeteer page error
                // eslint-disable-next-line unicorn/prefer-type-error -- not a TypeError
                throw new Error(`${httpStatus} at ${link}`);
            }
            return {
                status: () => httpStatus
            };
        });
        const browserMock = () => ({
            _ignoreHTTPSErrors: ignoreHTTPSErrors,
            newPage: () => ({
                close: () => {},
                goto: pageGotoMock
            })
        });
        return {
            browserMock,
            pageGotoMock
        };
    };

    const createAxiosMock = (httpStatus) => () => {
        if (Number.isNaN(Number(httpStatus))) {
            // If execution error, format to match axios error
            const error = new Error('execution error');
            error.code = httpStatus;
            throw error;
        } else if (httpStatus >= httpResponse.badRequest) {
            // If erroneous response, throw error with response to match axios
            const error = new Error('bad request');
            error.response = { status: httpStatus };
            throw error;
        }
        return { status: httpStatus };
    };

    // eslint-disable-next-line complexity -- Allow low limit
    const checkLinksWithMocks = async (
        link,
        ignoredLinks,
        invalidPage = false,

        httpStatus = httpResponse.ok,
        ignoreHTTPSErrors = false,
        checkWithBrowser = false
        // eslint-disable-next-line max-params -- allow for test helper
    ) => {
        const context = getContext(checkWithBrowser, ignoredLinks);
        const { checkLink } = createLinkChecker();

        if (invalidPage) {
            // This enables forcing an error to be thrown for various test cases
            delete context.page;
        }

        const { browserMock, pageGotoMock } = createBrowserMock(
            httpStatus,
            link,
            ignoreHTTPSErrors
        );
        const pageGetElementSpy = vi.spyOn(page, '$');
        const pageEvaluateSpy = vi.spyOn(page, 'evaluate');
        const pageUrlSpy = vi
            .spyOn(page, 'url')
            .mockImplementation(() =>
                link.includes('#') ? link.split('#')[0] : link
            );
        const pageBrowserMock = vi
            .spyOn(page, 'browser')
            .mockImplementation(browserMock);

        const axiosMock = createAxiosMock(httpStatus);
        const axiosHeadMock = vi
            .spyOn(axios, 'head')
            .mockImplementation(axiosMock);
        const axiosGetMock = vi
            .spyOn(axios, 'get')
            .mockImplementation(axiosMock);

        const result = await checkLink(context, link);

        return {
            axiosGetMock,
            axiosHeadMock,
            pageBrowserMock,
            pageEvaluateSpy,
            pageGetElementSpy,
            pageGotoMock,
            pageUrlSpy,
            result
        };
    };

    beforeAll(async () => {
        browser = await puppeteer.launch(puppeteerLaunchOptions);
        page = await browser.newPage();
        page.setUserAgent(userAgent);
        await page.goto(testUrlLink, { waitUntil: 'load' });
    });

    afterEach(() => {
        // Reset ignoreHTTPSErrors to initial state after each test
        // since browser created with options before all.
        // Using internal browser property since not exposed
        // eslint-disable-next-line no-underscore-dangle -- require for puppeteer API
        page.browser()._ignoreHTTPSErrors =
            puppeteerLaunchOptions.ignoreHTTPSErrors;
        vi.restoreAllMocks();
    });

    afterAll(async () => {
        await page.close();
        await browser.close();
    });

    describe('ignored links', () => {
        const ignoreLinksTestLink = 'https://this.is.a.test/';
        const normalizedIgnoreLinksTestLink = 'https://this.is.a.test';

        it('should return 100 if exact link is in ignored links', async () => {
            expect.assertions(1);
            const ignoredLinks = [normalizedIgnoreLinksTestLink];
            const { result } = await checkLinksWithMocks(
                ignoreLinksTestLink,
                ignoredLinks
            );
            expect(result).toBe(httpResponse.continue);
        });

        it('should return 100 if normalized link is in ignored links', async () => {
            expect.assertions(1);
            const ignoredLinks = [normalizedIgnoreLinksTestLink];
            const { result } = await checkLinksWithMocks(
                normalizedIgnoreLinksTestLink,
                ignoredLinks
            );
            expect(result).toBe(httpResponse.continue);
        });

        it('should not return 100 is link is not in ignored links', async () => {
            expect.assertions(1);
            const ignoredLinks = ['https://this.is.not.a.test/'];
            const { result } = await checkLinksWithMocks(
                ignoreLinksTestLink,
                ignoredLinks
            );
            expect(result).not.toBe(httpResponse.continue);
        });

        it('should not return 100 is ignored links are not specified', async () => {
            expect.assertions(1);
            const { result } = await checkLinksWithMocks(ignoreLinksTestLink);
            expect(result).not.toBe(httpResponse.continue);
        });
    });

    describe('same page links', () => {
        it('should return 200 for url with # and no element (top of page)', async () => {
            expect.assertions(1);
            const link = `${testUrlLink}#`;
            const { result } = await checkLinksWithMocks(link);
            expect(result).toBe(httpResponse.ok);
        });

        it('should return 200 for url with #top', async () => {
            expect.assertions(1);
            const link = `${testUrlLink}#top`;
            const { result } = await checkLinksWithMocks(link);
            expect(result).toBe(httpResponse.ok);
        });

        it('should check page for element if element specified', async () => {
            expect.assertions(1);
            const element = '#foo';
            const link = `${testUrlLink}${element}`;
            const { pageGetElementSpy } = await checkLinksWithMocks(link);
            expect(pageGetElementSpy).toHaveBeenCalledWith(element);
        });

        it('should return 200 if element found on page', async () => {
            expect.assertions(1);
            const element = '#linked';
            const link = `${testUrlLink}${element}`;
            const { result } = await checkLinksWithMocks(link);
            expect(result).toBe(httpResponse.ok);
        });

        it('should return 200 if escaped element found on page', async () => {
            expect.assertions(1);
            // Parentheses must be escaped in queryselector
            const element = '#this-is-linked-content-(also)';
            const link = `${testUrlLink}${element}`;
            const { result } = await checkLinksWithMocks(link);
            expect(result).toBe(httpResponse.ok);
        });

        it('should return #element not found error if element is not found on page', async () => {
            expect.assertions(1);
            const element = '#notlinked';
            const link = `${testUrlLink}${element}`;
            const { result } = await checkLinksWithMocks(link);
            expect(result).toBe(`${element} Not Found`);
        });

        it('should return 999 if error thrown while checking link', async () => {
            expect.assertions(1);
            const element = '#linked';
            const link = `${testUrlLink}${element}`;
            const { result } = await checkLinksWithMocks(link, [], true);
            expect(result).toBe(httpResponse.unknownError);
        });
    });

    describe('external page links with browser', () => {
        it('should check external link with browser if checkWithBrowser is true', async () => {
            expect.assertions(1);
            const { pageGotoMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.ok,
                false,
                true
            );
            expect(pageGotoMock).toHaveBeenCalledWith(externalLink);
        });

        it('should not check external link with browser if checkWithBrowser is false', async () => {
            expect.assertions(1);
            const { pageGotoMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.ok,
                false,
                false
            );
            expect(pageGotoMock).not.toHaveBeenCalled();
        });

        it('should return the HTTP response status for a nominal request', async () => {
            expect.assertions(1);
            const response = httpResponse.ok;
            const { result } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                response,
                false,
                true
            );
            expect(result).toBe(response);
        });

        it('should return the HTTP response status for an off-nominal request with a response', async () => {
            expect.assertions(1);
            const response = httpResponse.notFound;
            const { result } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                response,
                false,
                true
            );
            expect(result).toBe(response);
        });

        it('should return the error code without link for a request with an execution error', async () => {
            expect.assertions(2);
            const response = 'net::ERR_NAME_NOT_RESOLVED';
            const { result } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                response,
                false,
                true
            );
            expect(result).toBe(response);
            expect(result).not.toMatch(externalLink);
        });
    });

    describe('external page links', () => {
        it('should check external link if checkWithBrowser is false', async () => {
            expect.assertions(1);
            const { axiosHeadMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.ok,
                false,
                false
            );
            /* eslint-disable-next-line vitest/prefer-called-with -- Initial
               check that axios was called, later checks confirm arguments */
            expect(axiosHeadMock).toHaveBeenCalled();
        });

        it('should not check external link if checkWithBrowser is true', async () => {
            expect.assertions(1);
            const { axiosHeadMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.ok,
                false,
                true
            );
            expect(axiosHeadMock).not.toHaveBeenCalled();
        });

        it('should return error message if link has file: protocol', async () => {
            expect.assertions(1);
            const { result } = await checkLinksWithMocks(fileLink);
            expect(result).toBe('Cannot check "file:" URLs');
        });

        it('should set user agent to match the browser page', async () => {
            expect.assertions(2);
            const { axiosHeadMock } = await checkLinksWithMocks(externalLink);
            const axiosOptions = axiosHeadMock.mock.calls[0][1];
            expect(axiosOptions).toHaveProperty('headers');
            expect(axiosOptions.headers).toHaveProperty(
                'User-Agent',
                userAgent
            );
        });

        it('should add custom https agent if browser ignoreHTTPSErrors is true', async () => {
            expect.assertions(2);
            const { axiosHeadMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.ok,
                true
            );
            const axiosOptions = axiosHeadMock.mock.calls[0][1];
            expect(axiosOptions).toHaveProperty('httpsAgent');
            expect(axiosOptions.httpsAgent.options).toHaveProperty(
                'rejectUnauthorized',
                false
            );
        });

        it('should not add custom https agent if browser ignoreHTTPSErrors is false', async () => {
            expect.assertions(1);
            const { axiosHeadMock } = await checkLinksWithMocks(externalLink);
            const axiosOptions = axiosHeadMock.mock.calls[0][1];
            expect(axiosOptions).not.toHaveProperty('httpsAgent');
        });

        it('should make only a HEAD request for link if not same page and response is valid', async () => {
            expect.assertions(2);
            const { axiosHeadMock, axiosGetMock } =
                await checkLinksWithMocks(externalLink);
            expect(axiosHeadMock.mock.calls[0][0]).toBe(externalLink);
            expect(axiosGetMock).not.toHaveBeenCalled();
        });

        it('should make only a HEAD request for link if not same page and HEAD returns code 429', async () => {
            expect.assertions(2);
            const { axiosHeadMock, axiosGetMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.tooManyRequests
            );
            expect(axiosHeadMock.mock.calls[0][0]).toBe(externalLink);
            expect(axiosGetMock).not.toHaveBeenCalled();
        });

        it('should make only a HEAD request for link if not same page and HEAD returns an execution error', async () => {
            expect.assertions(2);
            const responseCode = 'ENOTFOUND';
            const { axiosHeadMock, axiosGetMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                responseCode
            );
            expect(axiosHeadMock.mock.calls[0][0]).toBe(externalLink);
            expect(axiosGetMock).not.toHaveBeenCalled();
        });

        it('should make only a HEAD request passing the current page user agent if response is valid', async () => {
            expect.assertions(2);
            const expectedHeaders = { headers: { 'User-Agent': userAgent } };
            const { axiosHeadMock, axiosGetMock } =
                await checkLinksWithMocks(externalLink);
            expect(axiosHeadMock.mock.calls[0][1]).toStrictEqual(
                expect.objectContaining(expectedHeaders)
            );
            expect(axiosGetMock).not.toHaveBeenCalled();
        });

        it('should make HEAD and GET request for link if not same page and HEAD returns error', async () => {
            expect.assertions(2);
            const { axiosHeadMock, axiosGetMock } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.badRequest
            );
            expect(axiosHeadMock.mock.calls[0][0]).toBe(externalLink);
            expect(axiosGetMock.mock.calls[0][0]).toBe(externalLink);
        });

        it('should make HEAD and GET request passing the current page user agent if HEAD returns error', async () => {
            expect.assertions(2);
            const expectedHeaders = { headers: { 'User-Agent': userAgent } };
            const testResults = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.badRequest
            );
            expect(testResults.axiosHeadMock.mock.calls[0][1]).toStrictEqual(
                expect.objectContaining(expectedHeaders)
            );
            expect(testResults.axiosGetMock.mock.calls[0][1]).toStrictEqual(
                expect.objectContaining(expectedHeaders)
            );
        });

        it('should return the HTTP response status for a nominal request', async () => {
            expect.assertions(1);
            const { result } = await checkLinksWithMocks(
                externalLink,
                [],
                false,
                httpResponse.ok
            );
            expect(result).toBe(httpResponse.ok);
        });

        it('should return the HTTP response status for an off-nominal request with a response', async () => {
            expect.assertions(1);
            const link = 'https://www.google.com/this.is.not.found/';
            const { result } = await checkLinksWithMocks(
                link,
                [],
                false,
                httpResponse.notFound
            );
            expect(result).toBe(httpResponse.notFound);
        });

        it('should return the error code for a request with an execution error', async () => {
            expect.assertions(1);
            const link = 'https://this.is.a.test/';
            const responseCode = 'ENOTFOUND';
            const { result } = await checkLinksWithMocks(
                link,
                [],
                false,
                responseCode
            );
            expect(result).toBe(responseCode);
        });
    });

    describe('ignore duplicates', () => {
        const checkDuplicateLinks = async (
            checkWithBrowser,
            ignoreDuplicates
        ) => {
            expect.assertions(4);

            const context = getContext(checkWithBrowser, [], ignoreDuplicates);
            const { checkLink } = createLinkChecker();

            // Set a response that would never be returned in case the URL is actually requested
            // (which would return 200). Could use an error status, but that would invoke the retry
            // logic, which would have to be mocked as well, and not relevant for this test.
            const httpStatus = 201;

            let linkCheckMock;
            if (checkWithBrowser) {
                // Mock puppeteer calls since used to check links
                const { browserMock, pageGotoMock } = createBrowserMock(
                    httpStatus,
                    externalLink,
                    false
                );
                vi.spyOn(page, 'browser').mockImplementation(browserMock);
                linkCheckMock = pageGotoMock;
            } else {
                // Mock axios calls since used to check links
                const axiosMock = createAxiosMock(httpStatus);
                const axiosHeadMock = vi
                    .spyOn(axios, 'head')
                    .mockImplementation(axiosMock);
                linkCheckMock = axiosHeadMock;
            }

            const result1 = await checkLink(context, externalLink);

            expect(linkCheckMock).toHaveBeenCalledTimes(1);
            expect(result1).toBe(httpStatus);

            const result2 = await checkLink(context, externalLink);

            // If ignoreDuplicates, then call count should not change (1),
            // otherwise increment (2).
            const secondCallCount = ignoreDuplicates ? 1 : 2;
            expect(linkCheckMock).toHaveBeenCalledTimes(secondCallCount);
            expect(result2).toBe(httpStatus);
        };

        it('should use cache for duplicate URLs if not using browser and ignoreDuplicates is true', async () => {
            expect.hasAssertions();
            const checkWithBrowser = false;
            const ignoreDuplicates = true;
            await checkDuplicateLinks(checkWithBrowser, ignoreDuplicates);
        });

        it('should not use cache for duplicate URLs if not using browser and ignoreDuplicates is false', async () => {
            expect.hasAssertions();
            const checkWithBrowser = false;
            const ignoreDuplicates = false;
            await checkDuplicateLinks(checkWithBrowser, ignoreDuplicates);
        });

        it('should use cache for duplicate URLs if using browser and ignoreDuplicates is true', async () => {
            expect.hasAssertions();
            const checkWithBrowser = true;
            const ignoreDuplicates = true;
            await checkDuplicateLinks(checkWithBrowser, ignoreDuplicates);
        });

        it('should not use cache for duplicate URLs if using browser and ignoreDuplicates is false', async () => {
            expect.hasAssertions();
            const checkWithBrowser = true;
            const ignoreDuplicates = false;
            await checkDuplicateLinks(checkWithBrowser, ignoreDuplicates);
        });
    });
});

describe('is failed response', () => {
    const response = { status: undefined };
    it('should return true for any numeric status value greater than or equal to 400', () => {
        expect.assertions(2);
        response.status = 400;
        expect(isFailedResponse(response)).toBe(true);
        response.status = 500;
        expect(isFailedResponse(response)).toBe(true);
    });

    it('return false for any numeric status value less than 400', () => {
        expect.assertions(2);
        response.status = 302;
        expect(isFailedResponse(response)).toBe(false);
        response.status = 200;
        expect(isFailedResponse(response)).toBe(false);
    });

    it('return true for any status value that is nor a number', () => {
        expect.assertions(2);
        response.status = 'ENOTFOUND';
        expect(isFailedResponse(response)).toBe(true);
        response.status = undefined;
        expect(isFailedResponse(response)).toBe(true);
    });
});

describe('normalize link', () => {
    const referenceUrl = 'https://www.test.com';

    it('should remove trailing slash without altering URL', () => {
        expect.assertions(1);
        const initialURL = `${referenceUrl}/`;
        expect(normalizeLink(initialURL)).toBe(referenceUrl);
    });

    it('should not strip www from domain', () => {
        expect.assertions(1);
        expect(normalizeLink(referenceUrl)).toBe(referenceUrl);
    });

    it('should add https as protocol if protocol relative URL', () => {
        expect.assertions(1);
        const initialURL = referenceUrl.replace('https:', '');
        expect(normalizeLink(initialURL)).toBe(referenceUrl);
    });

    it('should remove hash from url', () => {
        expect.assertions(1);
        const initialURL = `${referenceUrl}/#elementId`;
        expect(normalizeLink(initialURL)).toBe(referenceUrl);
    });
});
