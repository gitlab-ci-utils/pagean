import { describe, expect, it } from 'vitest';
import { bin } from 'bin-tester';
import stripAnsi from 'strip-ansi';

// These names do not use path.join since these names resolve in all OSs, and are
// included in snapshots. With path.join there would be a Linux/Windows mismatch
// in tests (the snapshots matching the OS they were created in).
const cliTestFolder = './tests/fixtures/configs/cli-tests';
const defaultConfigFileName = '.pageanrc.json';
const nonExistentConfigFileName = 'not-a-file.json';
const invalidConfigFileName =
    './tests/fixtures/configs/unit-tests/invalid.pageanrc.json';
const emptyConfigFileName =
    './tests/fixtures/configs/unit-tests/empty.pageanrc.json';
const allEmptyConfigFileName =
    './tests/fixtures/configs/cli-tests/all-empty.pageanrc.json';
const allErrorsConfigFileName =
    './tests/fixtures/configs/cli-tests/all-errors.pageanrc.json';

const errorMessage = 'Error linting pageanrc file';
const lintCommand = 'pageanrc-lint';

describe('pageanrc lint', () => {
    const testLintInvalidFiles = async (binArguments, message) => {
        expect.assertions(3);
        const result = await bin({
            binArguments,
            command: lintCommand
        });
        expect(result.code).toBe(1);
        expect(result.error.message).toMatch(message);
        expect(result.stderr).toMatch(errorMessage);
    };

    it('should return an error and exit for a non existent pageanrc file', async () => {
        expect.hasAssertions();
        const args = [nonExistentConfigFileName];
        await testLintInvalidFiles(args, 'ENOENT');
    });

    it('should return an error and exit for an invalid pageanrc file', async () => {
        expect.hasAssertions();
        const args = [invalidConfigFileName];
        await testLintInvalidFiles(args, 'Unexpected end of JSON input');
    });

    const testLintWithErrorSnapshot = async (binArguments) => {
        expect.assertions(1);
        const { stderr } = await bin({
            binArguments,
            command: lintCommand
        });
        // Strip ansi escape codes since not serialized in snapshots
        expect(stripAnsi(stderr)).toMatchSnapshot('stderr');
    };

    const testLintWithOutputSnapshot = async (binArguments) => {
        expect.assertions(1);
        const { stdout } = await bin({
            binArguments,
            command: lintCommand
        });
        expect(stdout).toMatchSnapshot('stdout');
    };

    it('should lint the pageanrc file specified as argument', async () => {
        expect.hasAssertions();
        const { stderr } = await bin({
            binArguments: [allErrorsConfigFileName],
            command: lintCommand
        });
        expect(stderr).toMatch(allErrorsConfigFileName);
    });

    it('should lint the default file if none specified', async () => {
        expect.assertions(1);
        const { stdout } = await bin({
            binArguments: [],
            command: lintCommand,
            workingDirectory: cliTestFolder
        });
        expect(stdout).toMatch(defaultConfigFileName);
    });

    it('should lint the all errors pageanrc and return all CLI errors', async () => {
        expect.hasAssertions();
        const args = [allErrorsConfigFileName];
        await testLintWithErrorSnapshot(args);
    });

    it('should lint the all empty pageanrc and return all CLI errors', async () => {
        expect.hasAssertions();
        const args = [allEmptyConfigFileName];
        await testLintWithErrorSnapshot(args);
    });

    it('should lint the empty pageanrc and return all CLI errors', async () => {
        expect.hasAssertions();
        const args = [emptyConfigFileName];
        await testLintWithErrorSnapshot(args);
    });

    it('should lint the all errors pageanrc and return all JSON errors to stdout with -j option', async () => {
        expect.hasAssertions();
        const args = ['-j', allErrorsConfigFileName];
        await testLintWithOutputSnapshot(args);
    });

    it('should lint the all empty pageanrc and return all JSON errors to stdout with --json option', async () => {
        expect.hasAssertions();
        const args = ['--json', allEmptyConfigFileName];
        await testLintWithOutputSnapshot(args);
    });
});
