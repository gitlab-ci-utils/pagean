import path from 'node:path';

import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';

import { __dirname, readJson } from '../lib/utils.js';
import testLogger from '../lib/logger.js';
import { testResultStates } from '../lib/test-utils.js';

const defaultConfig = await readJson(
    path.join(__dirname, '..', 'lib', 'default-config.json')
);
defaultConfig.project = 'Default project';

const configNoCli = {
    reporters: ['html', 'json']
};

const defaultUrl = 'https://somewhere.com';
const defaultTestName = 'should run a test';

const testResultSymbols = Object.freeze({
    failed: '×',
    pageError: ' ×',
    passed: '√',
    warning: '‼'
});

describe('startUrlTests', () => {
    let loggerNoCli, loggerWithCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        vi.restoreAllMocks();
    });

    it('should add URL results object to test results', () => {
        expect.assertions(3);
        // Console log is mocked to avoid writing output to the console during the test
        vi.spyOn(console, 'log').mockImplementation(() => {});

        const startingResults = loggerWithCli.getTestResults();
        expect(startingResults.results).toHaveLength(0);

        loggerWithCli.startUrlTests(defaultUrl);
        const endingResults = loggerWithCli.getTestResults();
        expect(endingResults.results).toHaveLength(1);
        expect(endingResults.results[0].url).toBe(defaultUrl);
    });

    it('should log to console with URL being tested if CLI reporter enabled', () => {
        expect.assertions(1);
        const consoleLogSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        loggerWithCli.startUrlTests(defaultUrl);
        expect(consoleLogSpy).toHaveBeenCalledWith(
            expect.stringContaining(defaultUrl)
        );
    });

    it('should not log to console with URL being tested if CLI reporter disabled', () => {
        expect.assertions(1);
        const consoleLogSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        loggerNoCli.startUrlTests(defaultUrl);
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });
});

describe('logPageError', () => {
    let loggerNoCli, loggerWithCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        vi.restoreAllMocks();
    });

    const defaultPageErrorMessage = 'Page failed to load';
    const testLogPageError = (message, logger) => {
        const consoleLogSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        logger.startUrlTests(defaultUrl);
        logger.logPageError(message);

        const allTestResults = logger.getTestResults();

        return { allTestResults, consoleLogSpy };
    };

    it('should log page error with provided message', () => {
        expect.assertions(1);
        const testResults = {
            pageError: defaultPageErrorMessage,
            tests: [],
            url: defaultUrl
        };
        const { allTestResults } = testLogPageError(
            defaultPageErrorMessage,
            loggerWithCli
        );
        expect(allTestResults.results[0]).toStrictEqual(testResults);
    });

    it('should increment page error in summary', () => {
        expect.assertions(1);
        const expectedPageErrorCount = 2;
        testLogPageError(defaultPageErrorMessage, loggerWithCli);
        const { allTestResults } = testLogPageError(
            defaultPageErrorMessage,
            loggerWithCli
        );
        expect(allTestResults.summary.pageError).toBe(expectedPageErrorCount);
    });

    it('should write page error to the console if CLI reporter enabled', () => {
        expect.assertions(2);
        const { consoleLogSpy } = testLogPageError(
            defaultPageErrorMessage,
            loggerWithCli
        );
        expect(consoleLogSpy.mock.calls).toHaveLength(3);
        expect(consoleLogSpy.mock.calls[1][0]).toMatch(defaultPageErrorMessage);
    });

    it('should not write page error to the console if CLI reporter disabled', () => {
        expect.assertions(2);
        const { consoleLogSpy } = testLogPageError(
            defaultPageErrorMessage,
            loggerNoCli
        );
        expect(consoleLogSpy.mock.calls).toHaveLength(1);
        expect(consoleLogSpy.mock.calls[0][0]).not.toMatch(
            defaultPageErrorMessage
        );
    });
});

describe('logTestResults', () => {
    let loggerNoCli, loggerWithCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        vi.restoreAllMocks();
    });

    // This includes only the console log from startUrlTests and getTestResults
    const consoleLoggerSpyDefaultCalls = 2;
    const testLogTestResults = (testResults, logger) => {
        const consoleLogSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        logger.startUrlTests(defaultUrl);
        logger.logTestResults(testResults);

        const allTestResults = logger.getTestResults();

        return { allTestResults, consoleLogSpy };
    };

    const testConsoleLogOutput = (testResult, logger) => {
        const consoleSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        logger.startUrlTests(defaultUrl);

        const testResults = {
            data: [],
            name: defaultTestName,
            result: testResult
        };
        logger.logTestResults(testResults);
        return consoleSpy;
    };

    it('should not log test results if no results provided', () => {
        expect.assertions(2);
        const { allTestResults, consoleLogSpy } = testLogTestResults(
            undefined,
            loggerWithCli
        );
        expect(allTestResults.results[0].tests).toHaveLength(0);
        expect(consoleLogSpy).toHaveBeenCalledTimes(
            consoleLoggerSpyDefaultCalls
        );
    });

    it('should log test results if provided', () => {
        expect.assertions(1);
        const testResults = {
            data: [],
            name: defaultTestName,
            result: testResultStates.passed
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.results[0].tests[0]).toBe(testResults);
    });

    it('should output test name to console if test results provided and CLI reporter enabled', () => {
        expect.assertions(1);
        const consoleSpy = testConsoleLogOutput(
            testResultStates.passed,
            loggerWithCli
        );

        expect(consoleSpy).toHaveBeenNthCalledWith(
            2,
            expect.stringContaining(defaultTestName)
        );
    });

    it('should not output test name to console if test results provided and CLI reporter disabled', () => {
        expect.assertions(1);
        const consoleSpy = testConsoleLogOutput(
            testResultStates.passed,
            loggerNoCli
        );
        expect(consoleSpy).not.toHaveBeenCalled();
    });

    const testConsoleLogTestResults = (testResult) => {
        expect.assertions(2);
        const consoleSpy = testConsoleLogOutput(testResult, loggerWithCli);
        expect(consoleSpy).toHaveBeenNthCalledWith(
            2,
            expect.stringContaining(testResult)
        );
        expect(consoleSpy).toHaveBeenNthCalledWith(
            2,
            expect.stringContaining(testResultSymbols[testResult])
        );
    };

    const testNoConsoleLogTestResults = (testResult) => {
        expect.assertions(1);
        const consoleSpy = testConsoleLogOutput(testResult, loggerNoCli);
        expect(consoleSpy).not.toHaveBeenCalled();
    };

    it('should output test results to console for a passed test if CLI reporter enabled', () => {
        expect.hasAssertions();
        testConsoleLogTestResults(testResultStates.passed);
    });

    it('should not output test results to console for a passed test if CLI reporter disabled', () => {
        expect.hasAssertions();
        testNoConsoleLogTestResults(testResultStates.passed);
    });

    it('should output test results to console for a warning test if CLI reporter enabled', () => {
        expect.hasAssertions();
        testConsoleLogTestResults(testResultStates.warning);
    });

    it('should not output test results to console for a warning test if CLI reporter disabled', () => {
        expect.hasAssertions();
        testNoConsoleLogTestResults(testResultStates.warning);
    });

    it('should output test results to console for a failed test if CLI reporter enabled', () => {
        expect.hasAssertions();
        testConsoleLogTestResults(testResultStates.failed);
    });

    it('should not output test results to console for a failed test if CLI reporter disabled', () => {
        expect.hasAssertions();
        testNoConsoleLogTestResults(testResultStates.failed);
    });

    it('should increment only test and passed counters for a passed test', () => {
        expect.assertions(4);
        const testResults = {
            name: defaultTestName,
            result: testResultStates.passed
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.summary.tests).toBe(1);
        expect(allTestResults.summary.passed).toBe(1);
        expect(allTestResults.summary.warning).toBe(0);
        expect(allTestResults.summary.failed).toBe(0);
    });

    it('should increment only test and failed counters for a failed test', () => {
        expect.assertions(4);
        const testResults = {
            name: defaultTestName,
            result: testResultStates.failed
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.summary.tests).toBe(1);
        expect(allTestResults.summary.passed).toBe(0);
        expect(allTestResults.summary.warning).toBe(0);
        expect(allTestResults.summary.failed).toBe(1);
    });

    it('should increment only test and warning counters for a warning test', () => {
        expect.assertions(4);
        const testResults = {
            name: defaultTestName,
            result: testResultStates.warning
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.summary.tests).toBe(1);
        expect(allTestResults.summary.passed).toBe(0);
        expect(allTestResults.summary.warning).toBe(1);
        expect(allTestResults.summary.failed).toBe(0);
    });
});

describe('getTestResults', () => {
    let loggerNoCli, loggerWithCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        vi.restoreAllMocks();
    });

    const testGetTestResults = (includeTests, testName, testResult, logger) => {
        const consoleLogSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        if (includeTests) {
            logger.startUrlTests(defaultUrl);
            const name = testName;
            const result = testResult;
            logger.logTestResults({ name, result });
        }

        const results = logger.getTestResults();
        return {
            consoleLogSpy,
            results
        };
    };

    it('should return a results object with appropriate properties with no tests run', () => {
        expect.assertions(2);

        const { results } = testGetTestResults(
            false,
            undefined,
            undefined,
            loggerWithCli
        );
        expect(results).toStrictEqual(
            expect.objectContaining({
                executionStart: expect.any(Date),
                project: defaultConfig.project,
                results: expect.any(Array),
                summary: {
                    failed: 0,
                    pageError: 0,
                    passed: 0,
                    tests: 0,
                    warning: 0
                }
            })
        );
        expect(results.results).toHaveLength(0);
    });

    it('should return a results object with appropriate properties with tests run', () => {
        expect.assertions(3);

        const testName = defaultTestName;
        const testResult = testResultStates.passed;
        const { results } = testGetTestResults(
            true,
            testName,
            testResult,
            loggerWithCli
        );

        expect(results).toStrictEqual(
            expect.objectContaining({
                executionStart: expect.any(Date),
                project: defaultConfig.project,
                results: expect.any(Array),
                summary: {
                    failed: 0,
                    pageError: 0,
                    passed: 1,
                    tests: 1,
                    warning: 0
                }
            })
        );
        expect(results.results).toHaveLength(1);
        expect(results.results[0]).toStrictEqual(
            expect.objectContaining({
                tests: [{ name: testName, result: testResult }],
                url: defaultUrl
            })
        );
    });

    const testTestResultsSummary = (logger, messageCount) => {
        expect.assertions(4);

        const testResult = testResultStates.warning;
        const { consoleLogSpy } = testGetTestResults(
            true,
            defaultTestName,
            testResult,
            logger
        );

        const message = consoleLogSpy.mock.calls[messageCount - 1][0];
        expect(message).toMatch(/Tests: 1/);
        expect(message).toMatch(/Passed: 0/);
        expect(message).toMatch(/Warning: 1/);
        expect(message).toMatch(/Failed: 0/);
    };

    it('should log to console the test result summary with total, passed, failed, and warning counts if CLI reporter enabled', () => {
        expect.hasAssertions();
        // Expect test name, test result, and the final results messages
        const messageCount = 3;
        testTestResultsSummary(loggerWithCli, messageCount);
    });

    it('should log to console the test result summary with total, passed, failed, and warning counts if CLI reporter disabled', () => {
        expect.hasAssertions();
        // Expect only the final results message
        const messageCount = 1;
        testTestResultsSummary(loggerNoCli, messageCount);
    });
});
