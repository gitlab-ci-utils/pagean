#!/usr/bin/env node

import { Levels, log } from 'ci-logger';
import { program } from 'commander';

import { executeAllTests } from '../index.js';
import getConfig from '../lib/config.js';
import { pkg } from '../lib/utils.js';

const defaultConfigFileName = './.pageanrc.json';

program
    .version(pkg.version)
    .option(
        '-c, --config <file>',
        'the path to the pagean configuration file',
        defaultConfigFileName
    )
    .parse(process.argv);
const options = program.opts();

try {
    const config = await getConfig(options.config);
    executeAllTests(config);
} catch (error) {
    log({
        errorCode: 1,
        exitOnError: true,
        level: Levels.Error,
        message: `Error executing pagean tests\n${error.message}`
    });
}
