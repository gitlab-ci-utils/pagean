import fs from 'node:fs';
import path from 'node:path';

import { beforeEach, describe, expect, it, vi } from 'vitest';

import { __dirname, readJson } from '../lib/utils.js';
import config, { lintConfigFile } from '../lib/config.js';

const defaultConfig = await readJson(
    path.join(__dirname, '..', 'lib', 'default-config.json')
);

const nonExistentConfigFileName = 'not-a-file.json';
const invalidConfigFileName =
    './tests/fixtures/configs/unit-tests/invalid.pageanrc.json';
const emptyConfigFileName =
    './tests/fixtures/configs/unit-tests/empty.pageanrc.json';
const emptyUrlsConfigFileName =
    './tests/fixtures/configs/unit-tests/empty-urls.pageanrc.json';
const emptyUrlValuesConfigFileName =
    './tests/fixtures/configs/unit-tests/empty-url-values.pageanrc.json';
const noTestSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/no-test-settings.pageanrc.json';
const globalTestSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/global-test-settings.pageanrc.json';
const testSpecificSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/test-specific-settings.pageanrc.json';
const globalAndTestSpecificSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/global-and-test-specific-settings.pageanrc.json';
const projectNoTestSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/project-no-test-settings.pageanrc.json';
const puppeteerNoTestSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/puppeteer-no-test-settings.pageanrc.json';
const puppeteerOverrideHeadlessSettingsConfigFileName =
    './tests/fixtures/configs/unit-tests/puppeteer-override-headless.pageanrc.json';
const urlTypesConfigFileName =
    './tests/fixtures/configs/unit-tests/url-types.pageanrc.json';
const allTestPropertiesConfigFileName =
    './tests/fixtures/configs/unit-tests/global-and-test-specific-settings-test-props.pageanrc.json';
const allShorthandConfigFileName =
    './tests/fixtures/configs/unit-tests/global-and-test-specific-settings-shorthand.pageanrc.json';
const reportersEmptyArrayConfigFileName =
    './tests/fixtures/configs/unit-tests/reporters-empty.pageanrc.json';
const reportersInvalidTypeConfigFileName =
    './tests/fixtures/configs/unit-tests/reporters-invalid-type.pageanrc.json';
const reportersNotArrayConfigFileName =
    './tests/fixtures/configs/unit-tests/reporters-not-array.pageanrc.json';
const reportersValidConfigFileName =
    './tests/fixtures/configs/unit-tests/reporters-valid.pageanrc.json';
const htmlhintrcInvalidConfigFileName =
    './tests/fixtures/configs/unit-tests/htmlhintrc-invalid.pageanrc.json';
const ignoredLinksDenormalizedConfigFileName =
    './tests/fixtures/configs/unit-tests/ignored-links-denormalized.pageanrc.json';
const sitemapFindNoReplaceConfigFileName =
    './tests/fixtures/configs/unit-tests/sitemap-find-no-replace.pageanrc.json';
const sitemapReplaceNoFindConfigFileName =
    './tests/fixtures/configs/unit-tests/sitemap-replace-no-find.pageanrc.json';
const sitemapMissingUrlConfigFileName =
    './tests/fixtures/configs/unit-tests/sitemap-missing-url.pageanrc.json';
const htmlhintrcValidConfigFileName =
    './tests/fixtures/configs/unit-tests/htmlhintrc-valid.pageanrc.json';
const cliDefaultConfigFileName =
    './tests/fixtures/configs/cli-tests/.pageanrc.json';
const sitemapWithUrlsConfigFileName =
    './tests/fixtures/configs/unit-tests/sitemap-with-urls.pageanrc.json';
const sitemapWithDuplicateUrlsConfigFileName =
    './tests/fixtures/configs/unit-tests/sitemap-with-duplicate-urls.pageanrc.json';
const sitemapWithoutUrlsConfigFileName =
    './tests/fixtures/configs/unit-tests/sitemap-without-urls.pageanrc.json';

describe('process config', () => {
    describe('invalid config', () => {
        const invalidConfigMessage = 'invalid pageanrc schema';

        it('should throw if config file not found', async () => {
            expect.assertions(1);
            await expect(() =>
                config(nonExistentConfigFileName)
            ).rejects.toThrow('ENOENT');
        });

        it('should throw if config file is not JSON', async () => {
            expect.assertions(1);
            await expect(() => config(invalidConfigFileName)).rejects.toThrow(
                'Unexpected end of JSON input'
            );
        });

        it('should throw if no URLs or sitemap specified in config file', async () => {
            expect.assertions(1);
            await expect(() => config(emptyConfigFileName)).rejects.toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if sitemap object with no url property specified in config file', async () => {
            expect.assertions(1);
            await expect(() =>
                config(sitemapMissingUrlConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });

        it('should throw if sitemap object with find property and no replace property specified in config file', async () => {
            expect.assertions(1);
            await expect(() =>
                config(sitemapFindNoReplaceConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });

        it('should throw if sitemap object with replace property and no find property specified in config file', async () => {
            expect.assertions(1);
            await expect(() =>
                config(sitemapReplaceNoFindConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });

        it('should throw if empty URL array specified in config file', async () => {
            expect.assertions(1);
            await expect(() => config(emptyUrlsConfigFileName)).rejects.toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if URL object with no url property specified in config file', async () => {
            expect.assertions(1);
            await expect(() =>
                config(emptyUrlValuesConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });

        it('should throw if reporters specified in config are not array', async () => {
            expect.assertions(1);
            await expect(() =>
                config(reportersNotArrayConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });

        it('should throw if reporters specified in config are empty array', async () => {
            expect.assertions(1);
            await expect(() =>
                config(reportersEmptyArrayConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });

        it('should throw if reporters array specified in config has invalid values', async () => {
            expect.assertions(1);
            await expect(() =>
                config(reportersInvalidTypeConfigFileName)
            ).rejects.toThrow(invalidConfigMessage);
        });
    });

    const checkConfigAgainstSnapshot = async (configFileName, property) => {
        expect.assertions(1);
        const processedConfig = await config(configFileName);
        expect(processedConfig[property]).toMatchSnapshot();
    };

    describe('project name', () => {
        it('should return config with project name if provided', async () => {
            expect.assertions(1);
            const processedConfig = await config(
                projectNoTestSettingsConfigFileName
            );
            expect(processedConfig.project).toBe('This is a test project');
        });

        it('should return config with empty project name if none provided', async () => {
            expect.assertions(1);
            const processedConfig = await config(
                puppeteerNoTestSettingsConfigFileName
            );
            expect(processedConfig.project).toBe('');
        });
    });

    describe('puppeteer launch options', () => {
        it('should return config with default and config puppeteer launch settings', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                puppeteerNoTestSettingsConfigFileName,
                'puppeteerLaunchOptions'
            );
        });

        it('should return config with the default puppeteer launch settings', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                noTestSettingsConfigFileName,
                'puppeteerLaunchOptions'
            );
        });

        it('should return config with the puppeteer headless setting true', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                puppeteerOverrideHeadlessSettingsConfigFileName,
                'puppeteerLaunchOptions'
            );
        });
    });

    describe('test settings', () => {
        it('should return config with default test settings', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                noTestSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with global test settings', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                globalTestSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with default and test-specific settings', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                testSpecificSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with default, global, and test-specific settings', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                globalAndTestSpecificSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with all url types properly parsed', async () => {
            expect.assertions(6);
            const processedConfig = await config(urlTypesConfigFileName);
            // Case 1: Local file that exists.  Use regex since path change in different environments.
            expect(processedConfig.urls[0].url).toMatch(
                /file:\/{3}.*\/tests\/fixtures\/site\/consoleLog.html/
            );
            // Case 2: Local file that does not exist, so assume URL (add https://)
            expect(processedConfig.urls[1].url).toBe(
                'https://tests/fixtures/site/bar.html'
            );
            // Case 3: URL with no protocol (add https://)

            expect(processedConfig.urls[2].url).toBe(
                'https://localhost/foo.html'
            );
            // Case 4: URL with port and no protocol (add https://)

            expect(processedConfig.urls[3].url).toBe(
                'https://localhost:3000/foo.html'
            );
            // Case 5: URL with http protocol (unchanged)

            expect(processedConfig.urls[4].url).toBe(
                'http://localhost/foo.html'
            );
            // Case 6: URL with https protocol (unchanged)
            // eslint-disable-next-line vitest/max-expects -- index, 6 URLs tested
            expect(processedConfig.urls[5].url).toBe(
                'https://localhost/foo.html'
            );
        });

        it('should return the same config with test shorthand and property declaration', async () => {
            expect.assertions(1);
            const shorthandConfig = await config(allShorthandConfigFileName);
            const testPropertiesConfig = await config(
                allTestPropertiesConfigFileName
            );
            expect(shorthandConfig).toStrictEqual(testPropertiesConfig);
        });

        it('should normalize ignored links in brokenLinkTest settings', async () => {
            expect.assertions(2);
            // Original link: https://this.url.is.ignored/
            const normalizedLinks = ['https://this.url.is.ignored'];
            const globalSettingUrl = 0; // URL inherits global settings
            const testSettingUrl = 1; // URL has test-specific settings
            const normalizedLinksConfig = await config(
                ignoredLinksDenormalizedConfigFileName
            );
            expect(
                normalizedLinksConfig.urls[globalSettingUrl].settings
                    .brokenLinkTest.ignoredLinks
            ).toStrictEqual(normalizedLinks);
            expect(
                normalizedLinksConfig.urls[testSettingUrl].settings
                    .brokenLinkTest.ignoredLinks
            ).toStrictEqual(normalizedLinks);
        });
    });

    describe('htmlhint settings', () => {
        const defaultHtmlHintConfigFilename = './.htmlhintrc';
        const invalidHtmlHintConfigFilename = './nonexistent/.htmlhintrc';

        beforeEach(() => {
            vi.restoreAllMocks();
        });

        it('should check for default htmlhintrc file if none specified in config', async () => {
            expect.assertions(1);
            const fsExistsSpy = vi
                .spyOn(fs, 'existsSync')
                .mockImplementation(() => false);
            await config(globalAndTestSpecificSettingsConfigFileName);
            expect(fsExistsSpy).toHaveBeenCalledWith(
                defaultHtmlHintConfigFilename
            );
        });

        it('should check for htmlhintrc file from config if specified', async () => {
            expect.assertions(1);
            const fsExistsSpy = vi
                .spyOn(fs, 'existsSync')
                .mockImplementation(() => false);
            await config(htmlhintrcInvalidConfigFileName);
            expect(fsExistsSpy).toHaveBeenCalledWith(
                invalidHtmlHintConfigFilename
            );
        });

        it('should return valid htmlhint settings from file specified in config if file exists', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                htmlhintrcValidConfigFileName,
                'htmlHintConfig'
            );
        });

        it('should return valid default htmlhint settings if .htmlhintrc file exists and none specified in config', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                globalAndTestSpecificSettingsConfigFileName,
                'htmlHintConfig'
            );
        });

        it('should return no htmlhint config if htmlhintrc file does not exist', async () => {
            expect.assertions(1);
            const result = await config(htmlhintrcInvalidConfigFileName);
            expect(result.htmlHintConfig).toBeUndefined();
        });
    });

    describe('reporter settings', () => {
        it('should return default reporters if none specified in config', async () => {
            expect.assertions(1);
            const processedConfig = await config(
                globalTestSettingsConfigFileName
            );
            expect(processedConfig).toStrictEqual(
                expect.objectContaining({ reporters: defaultConfig.reporters })
            );
        });

        it('should return reporters from config if valid', async () => {
            expect.assertions(1);
            const expectedReporters = ['cli', 'html'];
            const processedConfig = await config(reportersValidConfigFileName);
            expect(processedConfig).toStrictEqual(
                expect.objectContaining({ reporters: expectedReporters })
            );
        });
    });

    describe('sitemap settings', () => {
        it('should populate urls with sitemap if no urls specified in config', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                sitemapWithoutUrlsConfigFileName,
                'urls'
            );
        });

        it('should add sitemap urls to config urls', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                sitemapWithUrlsConfigFileName,
                'urls'
            );
        });

        it('should add sitemap missing urls to urls specified in config', async () => {
            expect.hasAssertions();
            await checkConfigAgainstSnapshot(
                sitemapWithDuplicateUrlsConfigFileName,
                'urls'
            );
        });
    });
});

describe('lint config', () => {
    it('should throw if config file not found', () => {
        expect.assertions(1);
        expect(() => lintConfigFile(nonExistentConfigFileName)).toThrow(
            'ENOENT'
        );
    });

    it('should throw if config file is not JSON', () => {
        expect.assertions(1);
        expect(() => lintConfigFile(invalidConfigFileName)).toThrow(
            'Unexpected end of JSON input'
        );
    });

    it('should return an object with a boolean isValid property', () => {
        expect.assertions(1);
        const results = lintConfigFile(cliDefaultConfigFileName);
        expect(results).toStrictEqual(
            expect.objectContaining({ isValid: expect.any(Boolean) })
        );
    });

    it('should return an object with an errors array', () => {
        expect.assertions(1);
        const results = lintConfigFile(cliDefaultConfigFileName);
        expect(results).toStrictEqual(
            expect.objectContaining({ errors: expect.any(Array) })
        );
    });

    it('should return an object with isValid true if schema is valid', () => {
        expect.assertions(1);
        const results = lintConfigFile(cliDefaultConfigFileName);
        expect(results.isValid).toBe(true);
    });

    it('should return an object with isValid false if schema is invalid', () => {
        expect.assertions(1);
        const results = lintConfigFile(emptyUrlsConfigFileName);
        expect(results.isValid).toBe(false);
    });

    it('should return an object with errors array populated if schema is invalid', () => {
        expect.assertions(1);
        const results = lintConfigFile(emptyUrlsConfigFileName);
        expect(results.errors).toMatchSnapshot();
    });
});
