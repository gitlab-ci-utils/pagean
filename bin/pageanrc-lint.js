#!/usr/bin/env node

import { Levels, log } from 'ci-logger';
import kleur from 'kleur';
import { program } from 'commander';

import { formatErrors } from '../lib/schema-errors.js';
import { lintConfigFile } from '../lib/config.js';
import { pkg } from '../lib/utils.js';

const defaultConfigFileName = './.pageanrc.json';

program
    .version(pkg.version)
    .option('-j, --json', 'output JSON with full details')
    .description('Lint a pageanrc file')
    .usage(`[options] [file] (default: "${defaultConfigFileName}")`)
    .allowExcessArguments()
    .parse(process.argv);
const options = program.opts();

const logError = (message, exitOnError = true) => {
    log({ errorCode: 1, exitOnError, level: Levels.Error, message });
};

const outputJsonResults = (configFileName, lintResults) => {
    // Log JSON to stdout for consistency, and eliminates extraneous stderr
    // output in PowerShell which produces invalid JSON if piped to file.
    // eslint-disable-next-line no-magic-numbers -- no-magic-numbers - index
    log({ message: JSON.stringify(lintResults.errors, undefined, 2) });
    if (!lintResults.isValid) {
        logError(`Errors found in file ${configFileName}`);
    }
};

const outputConsoleResults = (configFileName, lintResults) => {
    if (lintResults.isValid) {
        log({ message: `\n${configFileName}: ${kleur.green('valid')}\n` });
    } else {
        logError(`\n${kleur.underline(configFileName)}`, false);
        for (const error of formatErrors(lintResults.errors)) {
            logError(error, false);
        }
        // Add line for spacing and error exits process
        logError('');
    }
};

try {
    const configFileName =
        program.args.length > 0 ? program.args[0] : defaultConfigFileName;
    const lintResults = lintConfigFile(configFileName);

    if (options.json) {
        outputJsonResults(configFileName, lintResults);
    } else {
        outputConsoleResults(configFileName, lintResults);
    }
} catch (error) {
    logError(`Error linting pageanrc file\n${error.message}`);
}
