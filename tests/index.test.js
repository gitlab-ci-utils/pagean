import { afterEach, describe, expect, it, vi } from 'vitest';

import * as fileUtils from '../lib/external-file-utils.js';
import * as reporter from '../lib/reporter.js';
import configParser from '../lib/config.js';
import { executeAllTests } from '../index.js';
import path from 'node:path';
import { testResultStates } from '../lib/test-utils.js';

const integrationTestConfigs = path.join(
    './',
    'tests',
    'fixtures',
    'configs',
    process.platform === 'win32' ? 'integration-tests-win' : 'integration-tests'
);

describe('integration tests', () => {
    const allPassConfigFileName = path.join(
        integrationTestConfigs,
        'all-passing-tests.pageanrc.json'
    );
    const eachTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'each-test-fail.pageanrc.json'
    );
    const horizontalScrollbarTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'horizontal-scrollbar.pageanrc.json'
    );
    const consoleOutputTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'console-output.pageanrc.json'
    );
    const consoleErrorTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'console-error.pageanrc.json'
    );
    const consoleErrorTestFailReporterHtmlConfigFileName = path.join(
        integrationTestConfigs,
        'console-error-reporter-html.pageanrc.json'
    );
    const consoleErrorTestFailReporterCliJsonConfigFileName = path.join(
        integrationTestConfigs,
        'console-error-reporter-cli-json.pageanrc.json'
    );
    const renderedHtmlTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'html-error.pageanrc.json'
    );
    const pageErrorConfigFileName = path.join(
        integrationTestConfigs,
        'page-error.pageanrc.json'
    );
    const pageLoadTimeTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'page-load-time.pageanrc.json'
    );
    const externalScriptTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'external-scripts-error.pageanrc.json'
    );
    const brokenLinksTestFailConfigFileName = path.join(
        integrationTestConfigs,
        'broken-links-error.pageanrc.json'
    );
    const brokenLinksTestBrowserFailConfigFileName = path.join(
        integrationTestConfigs,
        'broken-links-error-browser.pageanrc.json'
    );
    const brokenLinksTestCompressedPassingConfigFileName = path.join(
        integrationTestConfigs,
        'broken-links-passing-compressed.pageanrc.json'
    );
    const sitemapWithoutUrlsFailConfigFileName = path.join(
        integrationTestConfigs,
        'sitemap-without-urls.pageanrc.json'
    );
    const sitemapWithUrlsFailConfigFileName = path.join(
        integrationTestConfigs,
        'sitemap-with-urls.pageanrc.json'
    );

    const horizontalScrollbarTestName =
        'should not have a horizontal scrollbar';
    const consoleOutputTestName = 'should not have console output';
    const consoleErrorTestName = 'should not have console errors';
    const renderedHtmlTestName = 'should have valid rendered HTML';
    const pageLoadTimeTestName = 'should load page within 2 sec';
    const externalScriptTestName = 'should not have external scripts';
    const brokenLinksTestName = 'should not have broken links';

    const pageanErrorExitCode = 2;

    const executeTests = async (configFileName) => {
        const config = await configParser(configFileName);

        const consoleLogSpy = vi
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        const processSpy = vi
            .spyOn(process, 'exit')
            .mockImplementation(() => {});
        const reporterSpy = vi
            .spyOn(reporter, 'saveReports')
            .mockImplementation(() => {});
        const saveScriptSpy = vi
            .spyOn(fileUtils, 'saveExternalScript')
            .mockImplementation(() => {});

        await executeAllTests(config);
        return {
            consoleLogSpy,
            processSpy,
            reporterSpy,
            saveScriptSpy
        };
    };

    const executePassingIntegrationTest = async (config) => {
        expect.assertions(3);
        const { processSpy, reporterSpy, saveScriptSpy } =
            await executeTests(config);
        expect(processSpy).not.toHaveBeenCalled();
        expect(reporterSpy).toHaveBeenCalledTimes(1);
        expect(saveScriptSpy).not.toHaveBeenCalled();
    };

    const executeFailedIntegrationTest = async (
        config,
        testName,
        dataCount
    ) => {
        /* eslint-disable vitest/max-expects -- allow for integration test */
        expect.assertions(dataCount ? 7 : 6);
        const { processSpy, reporterSpy } = await executeTests(config);

        // Expect test suite to fail and exit with pagean error code
        expect(processSpy).toHaveBeenCalledWith(pageanErrorExitCode);
        expect(reporterSpy).toHaveBeenCalledTimes(1);

        const testResults = reporterSpy.mock.calls[0][0];
        // Expect only one URL to be tested
        expect(testResults.results).toHaveLength(1);
        const failedTests = testResults.results[0].tests;
        // Expect only one test to be run
        expect(failedTests).toHaveLength(1);
        const failedTest = failedTests[0];
        // Verify the correct test was run and failed
        expect(failedTest.name).toBe(testName);
        expect(failedTest.result).toBe(testResultStates.failed);
        if (dataCount) {
            expect(failedTest.data).toHaveLength(dataCount);
        }
        /* eslint-enable vitest/max-expects -- allow for integration test */
    };

    afterEach(() => {
        vi.restoreAllMocks();
    });

    it('should execute all tests and all should pass', async () => {
        expect.hasAssertions();
        await executePassingIntegrationTest(allPassConfigFileName);
    });

    it('should execute tests and horizontal scrollbar test should fail', async () => {
        expect.hasAssertions();
        await executeFailedIntegrationTest(
            horizontalScrollbarTestFailConfigFileName,
            horizontalScrollbarTestName
        );
    });

    it('should execute tests and console output test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 1;
        await executeFailedIntegrationTest(
            consoleOutputTestFailConfigFileName,
            consoleOutputTestName,
            dataCount
        );
    });

    it('should execute tests and console error test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 1;
        await executeFailedIntegrationTest(
            consoleErrorTestFailConfigFileName,
            consoleErrorTestName,
            dataCount
        );
    });

    it('should execute tests and rendered HTML test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 2;
        await executeFailedIntegrationTest(
            renderedHtmlTestFailConfigFileName,
            renderedHtmlTestName,
            dataCount
        );
    });

    it('should execute tests and page load time test should fail', async () => {
        expect.hasAssertions();
        await executeFailedIntegrationTest(
            pageLoadTimeTestFailConfigFileName,
            pageLoadTimeTestName
        );
    });

    it('should execute tests and external script test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 3;
        await executeFailedIntegrationTest(
            externalScriptTestFailConfigFileName,
            externalScriptTestName,
            dataCount
        );
    });

    it('should execute tests and broken links test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 3;
        await executeFailedIntegrationTest(
            brokenLinksTestFailConfigFileName,
            brokenLinksTestName,
            dataCount
        );
    });

    it('should execute tests and broken links test should pass with compressed response', async () => {
        expect.hasAssertions();
        await executePassingIntegrationTest(
            brokenLinksTestCompressedPassingConfigFileName
        );
    });

    it('should execute tests and broken links test (with browser) should fail', async () => {
        expect.hasAssertions();
        const dataCount = 2;
        await executeFailedIntegrationTest(
            brokenLinksTestBrowserFailConfigFileName,
            brokenLinksTestName,
            dataCount
        );
    });

    it('should execute all tests and report failure with the correct tests failed', async () => {
        expect.assertions(5);
        const configFileName = eachTestFailConfigFileName;
        const totalTests = 57;
        const externalScriptCount = 3;

        const { processSpy, reporterSpy, saveScriptSpy } =
            await executeTests(configFileName);

        expect(processSpy).toHaveBeenCalledWith(pageanErrorExitCode);
        expect(saveScriptSpy).toHaveBeenCalledTimes(externalScriptCount);

        const testResults = reporterSpy.mock.calls[0][0];
        // Verify number of expected tests were run
        expect(testResults.summary.tests).toBe(totalTests);
        // Verify appropriate tests failed.  Some tests have log data with times and paths that
        // will fail if just checking snapshot of results, so filter down to URL and failed tests.
        const failedTests = [];
        for (const testResult of testResults.results) {
            const failed = testResult.tests
                .filter((test) => test.result === testResultStates.failed)
                .map((test) => ({ name: test.name, url: testResult.url }));
            failedTests.push(...failed);
        }
        expect(failedTests).toMatchSnapshot();

        // Check that console message field names are correct and returning data from puppeteer.
        // These contain environment-specific data, so exact test isn't checked, but the fields
        // will be empty if they are no longer reported by Chromium (see #85, #86).

        const consoleLogTestFailResult = testResults.results[0].tests[1];
        expect(consoleLogTestFailResult.data[0]).toStrictEqual(
            expect.objectContaining({
                location: expect.any(Object),
                text: expect.any(String),
                type: expect.any(String)
            })
        );
    });

    it('should pass reporter config when saving reports', async () => {
        expect.assertions(1);
        const { reporterSpy } = await executeTests(
            consoleErrorTestFailReporterCliJsonConfigFileName
        );
        expect(reporterSpy.mock.calls[0][1]).toStrictEqual(['cli', 'json']);
    });

    it('should write all results to console if CLI reporter enabled', async () => {
        expect.assertions(1);
        const { consoleLogSpy } = await executeTests(
            consoleErrorTestFailReporterCliJsonConfigFileName
        );
        // Console should be called 3 times: url, test result, and final results

        expect(consoleLogSpy).toHaveBeenCalledTimes(3);
    });

    it('should only summary results to console if CLI reporter disabled', async () => {
        expect.assertions(1);
        const { consoleLogSpy } = await executeTests(
            consoleErrorTestFailReporterHtmlConfigFileName
        );
        // Console should only be called once for final results
        expect(consoleLogSpy).toHaveBeenCalledTimes(1);
    });

    it('should execute tests with sitemap (horizontal scrollbar and console error test should fail)', async () => {
        expect.assertions(1);
        const expectedResults = [
            'tests/fixtures/site/horizontalScrollbar.html',
            'tests/fixtures/site/scriptError404.html'
        ];

        const { reporterSpy } = await executeTests(
            sitemapWithoutUrlsFailConfigFileName
        );

        const results = reporterSpy.mock.calls[0][0].results.map(
            (result) => result.url
        );
        expect(results).toStrictEqual(expectedResults);
    });

    it('should execute tests with sitemap and URLs (horizontal scrollbar and console log/error test should fail)', async () => {
        expect.assertions(1);
        const expectedResults = [
            'tests/fixtures/site/consoleLog.html',
            'tests/fixtures/site/horizontalScrollbar.html',
            'tests/fixtures/site/scriptError404.html'
        ];

        const { reporterSpy } = await executeTests(
            sitemapWithUrlsFailConfigFileName
        );

        const results = reporterSpy.mock.calls[0][0].results.map(
            (result) => result.url
        );
        expect(results).toStrictEqual(expectedResults);
    });

    it('should log page error if URL fails to load', async () => {
        expect.assertions(5);
        const expectedResults = [
            'Testing URL: https://foo.test',
            'net::ERR_NAME_NOT_RESOLVED at https://foo.test',
            'Page Errors: 1'
        ];
        const { consoleLogSpy, processSpy } = await executeTests(
            pageErrorConfigFileName
        );
        expect(processSpy).toHaveBeenCalledWith(pageanErrorExitCode);
        expect(consoleLogSpy).toHaveBeenCalledTimes(3);
        expect(consoleLogSpy.mock.calls[0][0]).toMatch(expectedResults[0]);
        expect(consoleLogSpy.mock.calls[1][0]).toMatch(expectedResults[1]);
        expect(consoleLogSpy.mock.calls[2][0]).toMatch(expectedResults[2]);
    });
});
