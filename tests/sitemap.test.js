import { describe, expect, it } from 'vitest';

import { getUrlsFromSitemap } from '../lib/sitemap.js';

const defaultSitemap = { url: './tests/fixtures/sitemaps/default-sitemap.xml' };
const emptySitemap = { url: './tests/fixtures/sitemaps/empty-sitemap.xml' };
const invalidSitemap = { url: './tests/fixtures/sitemaps/invalid-sitemap.xml' };

const notExcludedUrls = [
    'https://somewhere.test/horizontalScrollbar.html',
    'https://somewhere.test/scriptError404.html'
];

const defaultUrls = [
    'https://somewhere.test/brokenLinks.html',
    'https://somewhere.test/duplicateLinks.html',
    ...notExcludedUrls
];

const replacedUrls = [
    'http://localhost:3000/brokenLinks.html',
    'http://localhost:3000/duplicateLinks.html',
    'http://localhost:3000/horizontalScrollbar.html',
    'http://localhost:3000/scriptError404.html'
];

describe('getUrlsFromSitemap', () => {
    it('should return an array', async () => {
        expect.assertions(1);
        const results = await getUrlsFromSitemap(defaultSitemap);
        expect(Array.isArray(results)).toBe(true);
    });

    it('returns an empty array if there are no URLs in the sitemap', async () => {
        expect.assertions(1);
        const results = await getUrlsFromSitemap(emptySitemap);
        expect(results).toStrictEqual([]);
    });

    it('should return an array of strings that are URLs when passed a URL', async () => {
        expect.assertions(3);
        const results = await getUrlsFromSitemap({
            url: 'https://pa11y.org/sitemap.xml'
        });
        expect(results).toBeInstanceOf(Array);
        expect(results.length).toBeGreaterThan(0);
        expect(results).toContain('https://pa11y.org/');
    });

    it('should return an array of strings that are URLs when passed a file path', async () => {
        expect.assertions(1);
        const results = await getUrlsFromSitemap(defaultSitemap);
        expect(results).toStrictEqual(defaultUrls);
    });

    it('should throw an error for an invalid sitemap', async () => {
        expect.assertions(1);
        await expect(getUrlsFromSitemap(invalidSitemap)).rejects.toThrow(
            'Error processing sitemap'
        );
    });

    it('should throw an error if no sitemap is provided', async () => {
        expect.assertions(1);
        await expect(getUrlsFromSitemap()).rejects.toThrow(
            'Error retrieving sitemap'
        );
    });

    it('should throw an error for a non-existent sitemap', async () => {
        expect.assertions(1);
        await expect(
            getUrlsFromSitemap('./tests/fixtures/sitemaps/non-existent.xml')
        ).rejects.toThrow('Error retrieving sitemap');
    });

    it('should return an array of URLs without exclusions if exclusions provided', async () => {
        expect.assertions(1);
        const sitemap = { ...defaultSitemap, exclude: ['Links'] };
        const results = await getUrlsFromSitemap(sitemap);
        expect(results).toStrictEqual(notExcludedUrls);
    });

    it('should return an array of URLs without update Urls if find/replace provided', async () => {
        expect.assertions(1);
        const sitemap = {
            ...defaultSitemap,
            find: 'https://somewhere.test',
            replace: 'http://localhost:3000'
        };
        const results = await getUrlsFromSitemap(sitemap);
        expect(results).toStrictEqual(replacedUrls);
    });
});
