/**
 * Functions for managing Pagean config files.
 *
 * @module config
 */

import fs from 'node:fs';
import path from 'node:path';

import Ajv from 'ajv/dist/2019.js';
import ajvErrors from 'ajv-errors';
import protocolify from 'protocolify';

import { __dirname, readJson } from './utils.js';
import { getUrlsFromSitemap } from './sitemap.js';
import { normalizeLink } from './link-utils.js';

const defaultConfig = await readJson(
    path.join(__dirname, 'default-config.json')
);
const defaultHtmlHintConfigFilename = './.htmlhintrc';

const getUrl = (testUrl) => {
    let rawUrl, urlSettings;
    if (typeof testUrl === 'object') {
        rawUrl = testUrl.url;
        urlSettings = testUrl.settings;
    } else {
        rawUrl = testUrl;
    }
    return { rawUrl, urlSettings };
};

const processTestSetting = (setting) => {
    if (typeof setting === 'object') {
        return setting;
    }
    return { enabled: setting };
};

const processAllTestSettings = (settings) => {
    if (!settings) {
        return;
    }
    const processedSettings = {};
    for (const key of Object.keys(settings)) {
        processedSettings[key] = processTestSetting(settings[key]);
    }
    /* eslint-disable-next-line consistent-return -- return undefined
       if no settings */
    return processedSettings;
};

const consolidateTestSettings = (
    defaultSettings,
    globalSettings,
    urlSettings
) => {
    const sanitizedGlobalSettings = globalSettings || {};
    const sanitizedUrlSettings = urlSettings || {};
    const processedSettings = {};
    for (const key of Object.keys(defaultSettings)) {
        processedSettings[key] = {
            ...defaultSettings[key],
            ...sanitizedGlobalSettings[key],
            ...sanitizedUrlSettings[key]
        };
    }
    if (processedSettings.brokenLinkTest.ignoredLinks) {
        processedSettings.brokenLinkTest.ignoredLinks =
            processedSettings.brokenLinkTest.ignoredLinks.map((link) =>
                normalizeLink(link)
            );
    }
    return processedSettings;
};

const addSitemapUrls = (config, sitemapUrls) => {
    if (config.urls) {
        for (const url of config.urls) {
            const { rawUrl } = getUrl(url);
            const index = sitemapUrls.indexOf(rawUrl);
            index !== -1 && sitemapUrls.splice(index, 1);
        }
        config.urls.push(...sitemapUrls);
    } else {
        config.urls = sitemapUrls;
    }
};

const processUrls = async (config) => {
    const processedConfigSettings = processAllTestSettings(config.settings);
    if (config.sitemap) {
        const sitemapUrls = await getUrlsFromSitemap(config.sitemap);
        addSitemapUrls(config, sitemapUrls);
    }
    return config.urls.map((testUrl) => {
        const { rawUrl, urlSettings } = getUrl(testUrl);
        return {
            rawUrl,
            settings: consolidateTestSettings(
                defaultConfig.settings,
                processedConfigSettings,
                processAllTestSettings(urlSettings)
            ),
            url: protocolify(rawUrl)
        };
    });
};

const getHtmlHintConfig = (htmlHintConfigFilename) => {
    // Allow users to specify a custom htmlhintrc file.
    // nosemgrep: eslint.detect-non-literal-fs-filename
    if (!fs.existsSync(htmlHintConfigFilename)) {
        return;
    }
    /* eslint-disable consistent-return -- return undefined if no settings */
    // Allow users to specify a custom htmlhintrc file.
    // nosemgrep: eslint.detect-non-literal-fs-filename
    return JSON.parse(fs.readFileSync(htmlHintConfigFilename, 'utf8'));
    /* eslint-enable consistent-return -- return undefined if no settings */
};

const validateConfigSchema = (config) => {
    const schema = JSON.parse(
        // All values hardcoded.
        // nosemgrep: eslint.detect-non-literal-fs-filename
        fs.readFileSync(
            path.join(__dirname, '..', 'schemas', 'pageanrc.schema.json')
        )
    );
    // Allow allErrors to lint the entire config file, although users could
    // ReDoS themselves.
    // nosemgrep: ajv-allerrors-true
    const ajv = new Ajv({ allErrors: true });
    ajvErrors(ajv);
    const validate = ajv.compile(schema);
    const isValid = validate(config);
    return { errors: validate.errors || [], isValid };
};

const getConfigFromFile = (configFileName) =>
    // Allow users to specify config filename.
    // nosemgrep: eslint.detect-non-literal-fs-filename
    JSON.parse(fs.readFileSync(configFileName, 'utf8'));

/** @typedef {import('puppeteer').LaunchOptions} LaunchOptions */

/**
 * @typedef  {object}        PageanConfig
 * @property {object}        htmlHintConfig         The htmlhint configuration.
 * @property {string}        project                The name of the project.
 * @property {LaunchOptions} puppeteerLaunchOptions Options for launching Puppeteer.
 * @property {string[]}      reporters              The reporters used to output results.
 * @property {object}        urls                   The URLs to analyze with specific settings.
 */

/**
 * Loads config from file and returns consolidated config with
 * defaults where values are not specified.
 *
 * @param   {string}                configFileName Pagean configuration file name.
 * @returns {Promise<PageanConfig>}                Consolidated Pagean configuration.
 * @throws  {TypeError}                            Throws if config file has an invalid schema.
 * @static
 * @public
 */
const processConfig = async (configFileName) => {
    const config = getConfigFromFile(configFileName);
    const { isValid } = validateConfigSchema(config);
    if (!isValid) {
        throw new TypeError(
            `File ${configFileName} has an invalid pageanrc schema`
        );
    }
    return {
        htmlHintConfig: getHtmlHintConfig(
            config.htmlhintrc || defaultHtmlHintConfigFilename
        ),
        project: config.project || '',
        puppeteerLaunchOptions: {
            ...defaultConfig.puppeteerLaunchOptions,
            ...config.puppeteerLaunchOptions
        },
        reporters: config.reporters || defaultConfig.reporters,
        urls: await processUrls(config)
    };
};

/**
 * Lints the configuration file schema.
 *
 * @param   {string}  configFileName Pagean configuration file name.
 * @returns {boolean}                True if file has valid Pagean config schema.
 * @static
 */
const lintConfigFile = (configFileName) => {
    const config = getConfigFromFile(configFileName);
    return validateConfigSchema(config);
};

export default processConfig;
export { lintConfigFile };
