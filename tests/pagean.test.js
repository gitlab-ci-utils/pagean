import path from 'node:path';

import { describe, expect, it } from 'vitest';
import { bin } from 'bin-tester';

describe('pagean CLI', () => {
    const testDirectory = path.join(
        './',
        'tests',
        'fixtures',
        'configs',
        process.platform === 'win32' ? 'cli-tests-win' : 'cli-tests'
    );
    const defaultConfigFileName = '.pageanrc.json';
    const allFailConfigFileName = 'all-fail-cli.pageanrc.json';
    const invalidConfigFileName = 'no-urls.pageanrc.json';
    const passFailConfigFileName = 'pass-fail-cli.pageanrc.json';
    const missingConfigFileName = 'foo.pageanrc.json';
    const executionErrorExitCode = 1;
    const pageanErrorExitCode = 2;

    const executePageanBinTest = async (binArguments, expectedResults) => {
        expect.assertions(Object.keys(expectedResults).length);

        const results = await bin({
            binArguments,
            command: 'pagean',
            workingDirectory: testDirectory
        });

        expect(results.code).toBe(expectedResults.code);
        if (expectedResults.error) {
            expect(results.error.message).toMatch(expectedResults.error);
        }
        if (expectedResults.stderr) {
            expect(results.stderr).toMatch(expectedResults.stderr);
        }
        if (expectedResults.stdout) {
            expectedResults.stdout === 'SNAPSHOT'
                ? expect(results.stdout).toMatchSnapshot()
                : expect(results.stdout).toMatch(expectedResults.stdout);
        }
    };

    it(`should default to ${defaultConfigFileName} if no config file specified via CLI`, async () => {
        expect.hasAssertions();
        const args = [];
        const expectedResults = {
            code: 0,
            stdout: 'SNAPSHOT'
        };
        await executePageanBinTest(args, expectedResults);
    });

    it('should use correct config file if specified via CLI option (--config)', async () => {
        expect.hasAssertions();
        const args = ['--config', passFailConfigFileName];
        const expectedResults = {
            code: pageanErrorExitCode,
            stdout: 'SNAPSHOT'
        };
        await executePageanBinTest(args, expectedResults);
    });

    it('should use correct config file if specified via CLI option shorthand (-c)', async () => {
        expect.hasAssertions();
        const args = ['-c', allFailConfigFileName];
        const expectedResults = {
            code: pageanErrorExitCode,
            stdout: 'SNAPSHOT'
        };
        await executePageanBinTest(args, expectedResults);
    });

    it('should exit with error if specified config file cannot be found', async () => {
        expect.hasAssertions();
        const args = ['-c', missingConfigFileName];
        const expectedResults = {
            code: executionErrorExitCode,
            error: 'ENOENT',
            stderr: 'Error executing pagean tests'
        };
        await executePageanBinTest(args, expectedResults);
    });

    it('should exit with error if specified config file is invalid', async () => {
        expect.hasAssertions();
        const args = ['-c', invalidConfigFileName];
        const expectedResults = {
            code: executionErrorExitCode,
            error: 'invalid pageanrc schema',
            stderr: 'Error executing pagean tests'
        };
        await executePageanBinTest(args, expectedResults);
    });
});
