/**
 * Miscellaneous utilities.
 *
 * @module utils
 */

import { fileURLToPath } from 'node:url';
import path from 'node:path';
import { readFile } from 'node:fs/promises';

/**
 * The file containing the current module, using the CJS global name.
 *
 * @type {string}
 */
// eslint-disable-next-line no-underscore-dangle -- match CJS name
const __filename = fileURLToPath(import.meta.url);

/**
 * The directory containing the current module, using the CJS global name.
 *
 * @type {string}
 */
// eslint-disable-next-line no-underscore-dangle -- match CJS name
const __dirname = path.dirname(__filename);

/**
 * Reads a JSON file and returns the parsed data.
 *
 * @param   {string}          filepath The path to the JSON file to read.
 * @returns {Promise<object>}          The parsed JSON data.
 */
const readJson = async (filepath) => {
    const data = await readFile(filepath, 'utf8');
    return JSON.parse(data);
};

/**
 * The package.json file for this project.
 *
 * @type {object}
 */
const pkg = await readJson(path.join(__dirname, '..', 'package.json'));

export { __dirname, __filename, readJson, pkg };
