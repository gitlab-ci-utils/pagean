/**
 * Miscellaneous test utilities.
 *
 * @module test-utils
 */

const testResultStates = Object.freeze({
    failed: 'failed',
    passed: 'passed',
    warning: 'warning'
});

const getTestSettings = (testSettingProperty, urlSettings) => {
    if (urlSettings[testSettingProperty] === undefined) {
        throw new Error(
            `Property '${testSettingProperty}' could not be found in test settings`
        );
    }
    return urlSettings[testSettingProperty];
};

/**
 * Wrapper function for executing all Pagean tests that manages getting
 * test-specific settings from configuration, passing the test context,
 * and logging results.
 *
 * @param   {string}        name                The name of the test being run.
 * @param   {Function}      testFunction        The test function to be executed.
 * @param   {object}        testContext         The test execution context.
 * @param   {string}        testSettingProperty The name of the config property
 *                                              with settings for the current test.
 * @returns {Promise<void>}
 * @static
 */
const pageanTest = async (
    name,
    testFunction,
    testContext,
    testSettingProperty
) => {
    const testSettings = getTestSettings(
        testSettingProperty,
        testContext.urlSettings
    );
    testContext.testSettings = testSettings;
    if (testSettings.enabled) {
        let results;
        try {
            results = await testFunction(testContext);
        } catch (error) {
            results = { data: { error }, result: testResultStates.failed };
        }
        if (
            results.result === testResultStates.failed &&
            testSettings.failWarn
        ) {
            results.result = testResultStates.warning;
        }
        testContext.logger.logTestResults({
            name,
            ...results
        });
    }
};

export { testResultStates, pageanTest };
