/**
 * Manages Pagean reporting.
 *
 * @module reporter
 */

import fs from 'node:fs';
import path from 'node:path';

import handlebars from 'handlebars';

import { __dirname } from './utils.js';

const htmlReportTemplateName = 'report-template.handlebars';
const htmlReportFileName = './pagean-results.html';
const jsonReportFileName = './pagean-results.json';

handlebars.registerHelper('json', (context) =>
    // eslint-disable-next-line no-magic-numbers -- no-magic-numbers - count
    JSON.stringify(context, undefined, 2)
);

const saveHtmlReport = (results) => {
    const templateFile = path.resolve(
        path.join(__dirname, htmlReportTemplateName)
    );
    // Path hardcoded above, not from user input.
    // nosemgrep: eslint.detect-non-literal-fs-filename
    const htmlReportTemplate = fs.readFileSync(templateFile, 'utf8');
    const template = handlebars.compile(htmlReportTemplate);
    const htmlReport = template(results);
    fs.writeFileSync(htmlReportFileName, htmlReport);
};

const saveJsonReport = (results) => {
    fs.writeFileSync(jsonReportFileName, JSON.stringify(results));
};

const reporterTypes = Object.freeze({
    cli: 'cli',
    html: 'html',
    json: 'json'
});

/**
 * Outputs the given results with specified reporters.
 *
 * @param {object}   results   Consolidated Pagean results for a all tests.
 * @param {string[]} reporters The configured reporters.
 * @static
 */
const saveReports = (results, reporters) => {
    if (reporters.includes(reporterTypes.json)) {
        saveJsonReport(results);
    }
    if (reporters.includes(reporterTypes.html)) {
        saveHtmlReport(results);
    }
};

export { saveReports, reporterTypes };
