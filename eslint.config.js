import globals from 'globals';
import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended-esm.js';

export default [
    ...recommendedConfig,
    {
        files: ['lib/tests.js'],
        languageOptions: {
            globals: {
                ...globals.browser
            }
        },
        name: 'browser (tests)'
    },
    {
        ignores: [
            '.vscode/**',
            'archive/**',
            'node_modules/**',
            'coverage/**',
            'pagean-external-scripts/**'
        ],
        name: 'ignores'
    }
];
