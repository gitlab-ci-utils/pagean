import fs from 'node:fs';
import path from 'node:path';

import { afterEach, describe, expect, it, vi } from 'vitest';

import { __dirname, readJson } from '../lib/utils.js';
import { saveReports } from '../lib/reporter.js';

const { reporters: defaultReporters } = await readJson(
    path.join(__dirname, '..', 'lib', 'default-config.json')
);

const htmlReportFileName = './pagean-results.html';
const jsonReportRawFileName = 'pagean-results.json';
const jsonReportFileName = `./${jsonReportRawFileName}`;

describe('reporter', () => {
    const saveReportTest = (fileName, reporters, saveFile = true) => {
        const testResults = {};
        const writeFileSpy = vi
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});

        saveReports(testResults, reporters);

        if (saveFile) {
            expect(writeFileSpy).toHaveBeenCalledWith(
                fileName,
                expect.any(String)
            );
        } else {
            expect(writeFileSpy).not.toHaveBeenCalledWith(
                fileName,
                expect.any(String)
            );
        }
        return writeFileSpy.mock.calls[0][1];
    };

    afterEach(() => {
        vi.restoreAllMocks();
    });

    it('should save HTML report file if specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['html'];
        saveReportTest(htmlReportFileName, reporters, true);
    });

    it('should not save HTML report file if not specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['json'];
        saveReportTest(htmlReportFileName, reporters, false);
    });

    it('should save JSON report file if specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['json'];
        saveReportTest(jsonReportFileName, reporters, true);
    });

    it('should not save JSON report file if not specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['html'];
        saveReportTest(jsonReportFileName, reporters, false);
    });

    it('should save JSON report with valid JSON', () => {
        expect.assertions(2);
        const reportContents = saveReportTest(
            jsonReportFileName,
            defaultReporters
        );
        expect(() => JSON.parse(reportContents)).not.toThrow();
    });

    it('should write expected JSON and HTML reports', () => {
        expect.assertions(3);
        const reportCount = 2;
        const jsonReportFile = path.join(
            'tests',
            'fixtures',
            'site',
            jsonReportRawFileName
        );
        const testResults = JSON.parse(fs.readFileSync(jsonReportFile, 'utf8'));
        const writeFileSpy = vi
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});

        saveReports(testResults, defaultReporters);

        expect(writeFileSpy).toHaveBeenCalledTimes(reportCount);
        const jsonReport = writeFileSpy.mock.calls[0][1];
        expect(jsonReport).toMatchSnapshot('json report');

        const htmlReport = writeFileSpy.mock.calls[1][1];
        expect(htmlReport).toMatchSnapshot('html report');
    });
});
