# Pin to the node active LTS release
FROM registry.gitlab.com/gitlab-ci-utils/container-images/puppeteer:node-22.14.0-bookworm-slim@sha256:b3014b55c200c5818cd855f48d352b1fe97ee3adfccc7c199341cb2a2a524f5b

ENV NODE_ENV=production
ENV PUPPETEER_CACHE_DIR=/home/pptruser/.cache/puppeteer

# Image uses a lesser privileged account, but need root to
# install Puppeteer with npm dependencies
USER root

# Copy application files and install dependencies
WORKDIR /pagean
COPY . /pagean/

# Always install the latest version for standalone applications.
# Include serve and wait-on to be able to run globally.
# hadolint ignore=DL3016
RUN npm ci && \
  npm link && \
  npm install -g serve wait-on

# Return to original lesser privileged account
USER pptruser

LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/pagean"
LABEL org.opencontainers.image.title="Pagean"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/pagean"

CMD [ "pagean" ]
