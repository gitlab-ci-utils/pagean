/**
 * Utilities for checking external JavaScript files.
 *
 * @module external-file-utils
 */

import fs from 'node:fs';
import path from 'node:path';

import axios from 'axios';

const externalFilePath = 'pagean-external-scripts';

/**
 * Checks if the JavaScript is external to the page and should be saved.
 *
 * @param   {string}  script The URL of the JavaScript file.
 * @param   {string}  page   The URL of the current page.
 * @returns {boolean}        True if the script is external to the page.
 * @static
 */
const shouldSaveFile = (script, page) => {
    const validProtocols = ['http:', 'https:'];
    const scriptUrl = new URL(script);
    const pageUrl = new URL(page);
    return (
        scriptUrl.host !== pageUrl.host &&
        validProtocols.includes(scriptUrl.protocol)
    );
};

/**
 * @typedef  {object} ExternalScript
 * @property {string} url            The original script url.
 * @property {string} [localFile]    The path to the downloaded file.
 * @property {string} [message]      The error message if the script
 *                                   failed to download.
 */

/**
 * Loads the JavaScript file from the specified URL and
 * saves it to disc.
 *
 * @param   {string}                  script The URL of the JavaScript file.
 * @returns {Promise<ExternalScript>}        An object with original script
 *                                           URL and local file name.
 * @static
 */
const saveExternalScript = async (script) => {
    const result = { url: script };
    try {
        const scriptUrl = new URL(script);
        const pathName = path.join(
            externalFilePath,
            scriptUrl.hostname,
            scriptUrl.pathname
        );
        // Path generated above from URL, not from user input.
        // nosemgrep: eslint.detect-non-literal-fs-filename
        if (!fs.existsSync(pathName)) {
            // Axios will throw for any error response
            const response = await axios.get(script);
            // Path generated above from URL, not from user input.
            // nosemgrep: eslint.detect-non-literal-fs-filename
            fs.mkdirSync(path.dirname(pathName), { recursive: true });
            // Path generated above from URL, not from user input.
            // nosemgrep: eslint.detect-non-literal-fs-filename
            fs.writeFileSync(pathName, response.data);
        }
        result.localFile = pathName;
    } catch (error) {
        result.error = error.message;
    }
    return result;
};

export { saveExternalScript, shouldSaveFile };
